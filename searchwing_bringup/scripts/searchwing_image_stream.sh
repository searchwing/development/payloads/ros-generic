#!/bin/bash

# fill in here the hostname and port of the qgc instance you want to stream to
targetHost="searchwing-gcs-laptop"
port="5602"
sleeptime=1.3
upCheckSleepTime=10

function sendPhoto() {
  echo "$1"
  hostname=$(</etc/hostname)
  imageTime=$(stat -c '%y' "$1" | cut -c 12-19)
  text="$hostname $imageTime"

  gst-launch-1.0 \
    multifilesrc location="$1" loop=false start-index=0 stop-index=0 caps="image/jpeg,framerate=\(fraction\)1/2" ! \
    image/jpeg,width=1640,height=1232,type=video,framerate=1/2 ! identity ! jpegdec ! \
    videoscale ! videoconvert ! video/x-raw,width=1280,height=720 ! \
    textoverlay text="$2" valignment=top halignment=left font-desc="Sans, 72" color=0x80FF0000 ! \
    textoverlay text="${text}" valignment=bottom halignment=left font-desc="Sans, 35" color=0x80FF0000 ! \
    v4l2h264enc extra-controls="controls,video_bitrate=100000,video_bitrate_mode=1" ! 'video/x-h264,level=(string)4' ! \
    h264parse ! rtph264pay ! udpsink async=false sync=false host="${targetHost}" port="${port}"
}

while [ true ]; do
  #check connectivity to host
  if ping -q -c 1 -W 1 "${targetHost}" >/dev/null; then
    echo "host ${targetHost} is up"

    #send photos
    for i in {1..6}; do
      sendPhoto /data/bilder/cam-l_latest.jpg left
      sleep "${sleeptime}"
    done
    for i in {1..6}; do
      sendPhoto /data/bilder/cam-r_latest.jpg right
      sleep "${sleeptime}"
    done

  else
    echo "host ${targetHost} is down"
    sleep "${upCheckSleepTime}"
  fi

done
