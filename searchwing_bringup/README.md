# searchwing_bringup

Launchfiles to start the processing in production

## start

- in.launch launches all modules for input from drone
- proc.launch launches all modules to process and save images to hdd

## modules

### timesync

installed via ntpd-driver settings from

https://github.com/vooon/ntpd_driver#ntpd-configuration

while running ntpd stats available via

`ntpq -pn`

ntpd restart via

`sudo /etc/init.d/ntp restart`
