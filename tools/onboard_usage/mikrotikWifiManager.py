import re
import subprocess
import time

interface = "wlan1"
ssids = ["side", "forward"]
connection = "ssh admin@192.168.42.2"


def exec_cmd(cmd):
    return subprocess.Popen(
        connection + " " + cmd, shell=True, stdout=subprocess.PIPE
    ).stdout.read()


while True:
    cmd = "interface wireless print basic"
    ret = exec_cmd(cmd)
    a = ret.decode("utf-8")
    pattern = r'ssid="([^"]+)"'
    matches = re.findall(pattern, a)
    wlan2CurrentSSID = matches[0]
    print("Current SSID: {}".format(wlan2CurrentSSID))
    time.sleep(2)

    print("Run SCAN...")
    cmd = "interface wireless scan {} background=yes duration=4".format(interface)
    ret = exec_cmd(cmd)
    a = ret.decode("utf-8")
    ssidPowers = {}
    for ssid in ssids:
        ssidPowers[ssid] = []
    for line in a.splitlines():
        ssidFound = [ssid for ssid in ssids if (ssid in line)]
        if len(ssidFound) > 0:
            vals = re.findall(r"-([0-9]+([.]?[0-9]*))", line)
            if len(vals) > 0:
                val = -int(vals[0][0])
                ssidPowers[ssidFound[0]].append(val)
    for ssid in ssids:
        if len(ssidPowers[ssid]) > 0:
            ssidPowers[ssid] = ssidPowers[ssid][-1]
            print("{}: {}dbm".format(ssid, ssidPowers[ssid]))
    print(ssidPowers)
    try:
        maxSignalSSID = max(ssidPowers, key=ssidPowers.get)
        time.sleep(1)

        if maxSignalSSID != wlan2CurrentSSID:
            print("Connect to: {}!".format(maxSignalSSID))
            cmd = "/interface wireless set {} ssid={}".format(interface,maxSignalSSID)
            ret = exec_cmd(cmd)
    except:
        pass

    time.sleep(5)
