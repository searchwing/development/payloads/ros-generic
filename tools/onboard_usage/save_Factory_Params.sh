#!/bin/bash

#script to save the live params from the drone as factory params

# trap ctrl-c
trap 'echo "Restart searchwing* services..."; sudo systemctl start mavlink-router.service; sudo systemctl restart "searchwing*" --all; echo "Done!";' INT

echo "#################################"
echo "This script connects to the drone, downloads the current parameters and save the params in the folder ~/factory."
echo "These parameters are then used in the preflight parameters check."
echo "If you see the line 'Saved xxx parameters to mav.parm' param download is finished."
echo "Then you can abort the script using STRG+C."
echo "#################################"
sudo systemctl stop "searchwing*" --all
sudo systemctl stop mavlink-router.service

rm -rf /home/searchwing/factory
mkdir -p /home/searchwing/factory
cd /home/searchwing/factory || exit
mavproxy.py --master=/dev/ttyAMA0,500000
