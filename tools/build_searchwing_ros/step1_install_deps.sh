export ROS_DISTRO="noetic"
export ROS_OS_OVERRIDE=debian:buster
# I very strongly advise to set Python version used by ROS
# otherwise the packages will mix up python2 and python3 during build
# finally leading to the error you encountered (just like me before)
export ROS_PYTHON_VERSION="3"
# disable languages that we don't need
export ROS_LANG_DISABLE="geneus:genlisp:gennodejs"

rosdep install --from-paths ./src --rosdistro noetic -y --ignore-src --os=debian:buster -v

# for mavros
apt-get install libgeographic-dev -y
./src/mavros/mavros/scripts/install_geographiclib_datasets.sh

# for numpy on pi (see info in https://numpy.org/devdocs/user/troubleshooting-importerror.html#raspberry-pi)
apt-get install libatlas-base-dev -y
