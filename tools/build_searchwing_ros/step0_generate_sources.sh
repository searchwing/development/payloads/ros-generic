export ROS_DISTRO="noetic"
export ROS_OS_OVERRIDE=debian:buster
# I very strongly advise to set Python version used by ROS
# otherwise the packages will mix up python2 and python3 during build
# finally leading to the error you encountered (just like me before)
export ROS_PYTHON_VERSION="3"
# disable languages that I don't need
export ROS_LANG_DISABLE="geneus:genlisp:gennodejs"
sh -c 'echo "deb http://packages.ros.org/ros/ubuntu buster main" > /etc/apt/sources.list.d/ros-latest.list'
apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
apt-get update
apt-get install -y build-essential tmux cmake git
apt-get install -y python3-rosdep python3-rosinstall-generator python3-vcstool python3-empy python3-catkin-tools python3-osrf-pycommon python3-pip
apt-get install -y ca-certificates
rosdep init
rosdep update # run intentionlly as root even if not recommended
# Remove/add packages to get an installation covering your needs.
rosinstall_generator --rosdistro noetic --deps --tar \
  geometry \
  sensor_msgs \
  image_common \
  vision_opencv \
  image_transport \
  image_transport_plugins \
  image_pipeline \
  image_geometry \
  vision_msgs \
  compressed_image_transport \
  image_proc \
  mavros \
  mavros_extras |
  tee noetic.rosinstall

mkdir ./src

##import sources using vcs tool (instead of wstool)
vcs import --input noetic.rosinstall ./src
