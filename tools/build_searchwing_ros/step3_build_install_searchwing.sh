mkdir -p /opt/catkin_ws/src
cd /opt/catkin_ws/src || exit

export ROS_OS_OVERRIDE=debian:buster

git clone https://gitlab.com/searchwing/development/payloads/ros-generic
pip install -r ros-generic/requirements.txt --prefer-binary --system --force-reinstall

git clone https://gitlab.com/searchwing/development/payloads/ros-vision-generic
pip install -r ros-vision-generic/searchwing_pi/requirements.txt --prefer-binary --system --force-reinstall

cd /opt/catkin_ws/ || exit

source ~/.bashrc # needed for catkin tool

#install dependencies
echo "Install searchwing ros dependencies via rosdep..."
rosdep install --from-paths ./src --rosdistro noetic -y --ignore-src --os=debian:buster

#build searchwing ros sources
catkin init -w .
catkin config
catkin build -j8

#source built code
echo "source /opt/catkin_ws/devel/setup.bash" >>~/.bashrc

#install services
cp /opt/catkin_ws/src/ros-generic/searchwing_bringup/scripts/* /etc/systemd/system/
systemctl enable searchwing_roscore
systemctl enable searchwing_payload
systemctl enable searchwing_simple_webserver
systemctl enable searchwing_image_stream
systemctl enable searchwing_logrecorder
systemctl enable searchwing_rosbag

#setup mavlink router
mkdir -p /etc/mavlink-router
ln -sf /opt/catkin_ws/src/ros-generic/searchwing_bringup/config/mavlink-router-main.config /etc/mavlink-router/main.conf
systemctl enable mavlink-router
