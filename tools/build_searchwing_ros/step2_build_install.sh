export ROS_PYTHON_VERSION="3" # needed for mavlink package to choose correct python version

catkin config --install
catkin config --skiplist \
  stereo_image_proc \
  depth_image_proc \
  compressed_depth_image_transport \
  theora_image_transport \
  polled_camera \
  image_view \
  image_rotate \
  stereo_msgs

catkin build

#save ros logs to /data instead of /home/user/.ros
echo "export ROS_LOG_DIR='/data/ros'" >>/install/setup.bash

echo "source /install/setup.bash" >>~/.bashrc

#cleanup
#rm -rf build/
#rm -rf src/
#rm -rf devel/
#rm -rf logs/
