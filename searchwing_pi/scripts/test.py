json_str = '{"SensorTimestamp": 2310361850000, FocusFoM": 1119, "ExposureTime": 116045, "ColourTemperature": 4070, "AfState": 0, "SensorTemperature": 47.0, "AnalogueGain": 5.988304138183594, "AfPauseState": 0, "FrameDuration": 117307, "LensPosition": 1.0, "DigitalGain": 1.0079408884048462, "ColourCorrectionMatrix": (1.517957329750061, -0.4903615713119507, -0.027593854814767838, -0.30453723669052124, 1.4313663244247437, -0.12683095037937164, 0.016116781160235405, -0.648844838142395, 1.632733941078186), "AeLocked": False, "SensorBlackLevels": (4096, 4096, 4096, 4096), "Lux": 6.459288120269775, "ColourGains": (1.9347537755966187, 2.1411917209625244)}'
json_str = '{"SensorTimestamp": 1, "FocusFoM": 2}'
# , "ExposureTime": 116045, "ColourTemperature": 4070, "AfState": 0, "SensorTemperature": 47.0, "AnalogueGain": 5.988304138183594, "AfPauseState": 0, "FrameDuration": 117307, "LensPosition": 1.0, "DigitalGain": 1.0079408884048462, "ColourCorrectionMatrix": (1.517957329750061, -0.4903615713119507, -0.027593854814767838, -0.30453723669052124, 1.4313663244247437, -0.12683095037937164, 0.016116781160235405, -0.648844838142395, 1.632733941078186), "AeLocked": False, "SensorBlackLevels": (4096, 4096, 4096, 4096), "Lux": 6.459288120269775, "ColourGains": (1.9347537755966187, 2.1411917209625244)}'

# message = json_message_converter.convert_json_to_ros_message("std_msgs/String", json_str)
# print(message)

from diagnostic_msgs.msg import KeyValue

a = {"SensorTimestamp": 1, "FocusFoM": 2}

import rospy

rospy.init_node("picamera2", anonymous=True)

pub = rospy.Publisher("/meta", KeyValue, queue_size=3)

import random
import time

for i in range(1000):
    for key in a:
        val = a[key]
        msg = KeyValue()
        msg.key = key
        msg.value = str(random.random())
        pub.publish(msg)
        time.sleep(0.1)
