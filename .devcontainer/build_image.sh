#!/bin/bash

set -e

IMAGE_NAME=searchwing-ros-generic-"$(whoami)"

docker build . \
  --build-arg USERNAME="searchwing" \
  --build-arg USER_ID="$(id -u)" \
  --pull \
  -t "$IMAGE_NAME"
