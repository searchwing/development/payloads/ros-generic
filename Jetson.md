# Install Melodic on JetPack 4.3 with Python3

I manly follow this tutorial:

https://www.miguelalonsojr.com/blog/robotics/ros/python3/2019/08/20/ros-melodic-python-3-build.html

https://gist.github.com/drmaj/20b365ddd3c4d69e37c79b01ca17587a

## Install Tools

```bash
sudo apt install -y python3 python3-dev python3-pip build-essential
sudo -H pip3 install rosdep rospkg rosinstall_generator rosinstall wstool vcstools catkin_tools catkin_pkg
sudo rosdep init
rosdep update
```

## Prepare Workspace

```bash
sudo mkdir -p /opt/ros/melodic-py3

mkdir -p ~/catkin_ros_ws/src
cd catkin_ros_ws
catkin config --init -DCMAKE_BUILD_TYPE=Release --blacklist pcl_ros rviz_plugin_tutorials librviz_tutorial gazebo_plugins --install -i /opt/ros/melodic-py3


rosinstall_generator desktop_full --rosdistro melodic --deps --tar > melodic-desktop-full.rosinstall
wstool init -j8 src melodic-desktop-full.rosinstall
export ROS_PYTHON_VERSION=3
pip3 install cython numpy
pip3 install -U -f https://extras.wxpython.org/wxPython4/extras/linux/gtk3/ubuntu-18.04 wxPython
```

Create a file `install_skip`:

```bash
#!/bin/bash
#Call apt-get for each package
for pkg in "$@"
do
    echo "Installing $pkg"
    sudo apt-get -my install $pkg >> install.log
done
```

`chmod +x install_skip`

Ingore the errors that might occur here like, package not known:

```bash
sudo ./install_skip `rosdep check --from-paths src --ignore-src | grep python | sed -e "s/^apt\t//g" | sed -z "s/\n/ /g" | sed -e "s/python/python3/g"`
rosdep install --from-paths src --ignore-src -y --skip-keys="`rosdep check --from-paths src --ignore-src | grep python | sed -e "s/^apt\t//g" | sed -z "s/\n/ /g"`"
find . -type f -exec sed -i 's/\/usr\/bin\/env[ ]*python/\/usr\/bin\/env python3/g' {} +
```

Add what is still missing...

```bash
sudo apt-get install python3-empy python3-defusedxml python3-netifaces
```

*Other missing dependencies can be installed via apt later on, but please note them here for future
installs.*

**Replace cv_bridge with a fix for OpenCV4 compatibility**

```bash
wget https://github.com/fizyr-forks/vision_opencv/archive/opencv4.zip
rm -rf src/vision_opencv/cv_bridge
unzip opencv4.zip
mv vision_opencv-opencv4/cv_bridge src/vision_opencv
rm -rf vision_opencv-opencv4
```

**Replace image_transport_plugins for OpenCV4 compatibility**

```bash
rm -rf src/image_transport_plugins
git clone https://github.com/ros-perception/image_transport_plugins.git src/image_transport_plugins
```

```bash
wget https://github.com/ros-perception/image_pipeline/archive/noetic.zip
rm -rf src/image_pipeline/image_publisher
mv image_pipeline-noetic/image_publisher src/image_pipeline
```

**Build**

```bash
sudo sh -c 'export ROS_PYTHON_VERSION=3 && catkin build'
export PYTHONPATH=/usr/lib/python3/dist-packages
source /opt/ros/melodic-py3/setup.bash
```

**PostInstall**

After the install you can use the installation by sourcing it:

```bash
source /opt/ros/melodic-py3/setup.bash
# source /opt/ros/melodic-py3/setup.zsh
```

The PYTHONPATH will be automatically set by it.

If you use a second install dir where your own packages are build use this syntax:

```bash
source install/setuo.zsh --extend
```

# Install  Melodic on JetPack 4.3 using prebuilt version

**Install dependencies**

```bash
sudo apt install -y python3 python3-dev python3-pip build-essential
pip3 install rosdep rospkg rosinstall_generator rosinstall wstool vcstools catkin_tools catkin_pkg
sudo apt-get install python3-empy python3-defusedxml python3-netifaces
sudo apt-get install liblog4cxx-dev libboost-all-dev libconsole-bridge-dev python3-pycryptodome python3-gnupg libtinyxml2-dev libyaml-cpp0.5v5
```

**Install prebuilt ROS**

- Download prebuilt ROS Melodic `https://cloud.hs-augsburg.de/s/Z5X6SZsFbpkzTic`
- copy contents to `/opt/ros/melodic`
- follow setup steps from PostInstall of the instructions above
