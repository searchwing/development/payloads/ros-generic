import json
from typing import List

import numpy as np
import rospy

from .onnx_model_wrapper import OnnxModelWrapper
from .sliding_window_utils import Prediction, generate_sliding_window


class SlidingWindowDetector:
    def __init__(
        self,
        modelPath: str,
        thresh: float = 0.05,
        sizeX: int = 224,
        sizeY: int = 224,
        wrapperParams: str = "",
    ) -> None:
        rospy.logwarn(modelPath)
        # ToDo: is there a better simpler way to have a dict provided by the xml?
        if wrapperParams == "":
            wrapperParams = {}
        else:
            wrapperParams = json.loads(wrapperParams)
        self.model = OnnxModelWrapper(modelPath, **wrapperParams)
        self.sizeX = sizeX
        self.sizeY = sizeY
        self.thresh = thresh

    def predict(self, img: np.ndarray) -> List[Prediction]:
        tilesWithObjects = []
        for tile in generate_sliding_window(img, (self.sizeX, self.sizeY)):
            category, score = self.model.predict(tile.tile)
            if score > self.thresh:
                detection = Prediction(bbox=tile.bbox, category=category, score=score)
                tilesWithObjects.append(detection)
        return tilesWithObjects
