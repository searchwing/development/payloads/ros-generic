#!/usr/bin/env python3

import os

import rospy
from std_msgs.msg import Float64

tempSensor = "/sys/devices/platform/dht11@17/iio:device0/in_temp_input"
humSensor = "/sys/devices/platform/dht11@17/iio:device0/in_humidityrelative_input"

if not os.path.exists(tempSensor):
    rospy.logerr("Temperature-sensor doesnt exist:{}".format(tempSensor))
    rospy.logerr("Exit...")
    exit(0)
if not os.path.exists(humSensor):
    rospy.logerr("Humidity-sensor doesnt exist:{}".format(humSensor))
    rospy.logerr("Exit...")
    exit(0)

rospy.init_node("DhtNode")

tempPublisher = rospy.Publisher("/box/temperature", Float64, queue_size=2)
humidPublisher = rospy.Publisher("/box/humidity", Float64, queue_size=2)


def read_temp():
    try:
        f = open(tempSensor, "r")
        temp = float(f.read()) / 1000.0
        f.close()
        tempPublisher.publish(temp)
    except IOError as error:
        pass


def read_hum():
    try:
        f = open(humSensor, "r")
        hum = float(f.read()) / 1000.0
        f.close()
        humidPublisher.publish(hum)
    except IOError:
        pass


while not rospy.is_shutdown():
    rate = rospy.Rate(0.5)
    read_temp()
    read_hum()
    rate.sleep()

rospy.spin()
