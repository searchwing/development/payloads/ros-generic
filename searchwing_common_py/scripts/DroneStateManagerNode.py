#!/usr/bin/env python

import rospy
from mavros_msgs.msg import State
from std_msgs.msg import Bool, String

from searchwing_common_py.MissionManager import MissionManager
from searchwing_msgs.msg import DroneState

missionManager = MissionManager()

# start node
rospy.init_node("DroneStateManagerNode")


def callbackApmState(stateMsg):
    droneStateMsg = DroneState()
    droneStateMsg.header = stateMsg.header
    droneStateMsg.isFlying = Bool(stateMsg.armed)
    droneStateMsg.missionName = String(missionManager.processIsArmed(stateMsg.armed))
    droneStatePub.publish(droneStateMsg)


# subscriber
rospy.Subscriber("/mavros/state", State, callbackApmState)

# publisher
droneStatePub = rospy.Publisher("/searchwing/state", DroneState, queue_size=2)

rospy.spin()
