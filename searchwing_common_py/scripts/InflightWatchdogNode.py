#!/usr/bin/env python

import os
from os import listdir

import rospy
from mavros_msgs.msg import DebugValue, StatusText
from std_msgs.msg import Float64, Int16

from searchwing_msgs.msg import DroneState

rospy.init_node("InflightWatchdogNode")


class InflightWatchdog:
    def __init__(self) -> None:

        # Get Launch Parameters
        self.humidityThreshold = rospy.get_param(
            "~humidityThreshold", 85.0
        )  # threshold, for to high humidity
        self.cpuTempThreshold = rospy.get_param(
            "~cpuTempThreshold", 79.0
        )  # threshold, to high cpu temp
        self.boxTempThreshold = rospy.get_param(
            "~boxTempThreshold", 85.0
        )  # threshold for to high box temperature
        self.informGCSInterval = rospy.get_param(
            "~informGCSInterval", 5
        )  # inform every x minutes GCS
        self.imageCountInterval = rospy.get_param(
            "~imageCountInterval", 5
        )  # count images every 5 seconds
        self.imageCountWatchdogInterval = rospy.get_param(
            "~imageCountWatchdogInterval", 60
        )  # check every 60 seconds if images are taken
        self.sendDebugValueInterval = rospy.get_param(
            "~sendDebugValueInterval", 10
        )  # inteval in which debug values are send
        # Current drone State / Mission state
        self.droneStateMessage = None
        self.missionName = ""

        # taken images of current Mission
        self.currentImageCountL = 0
        self.currentImageCountR = 0

        # control value for Watchdog
        self.lastImageCountR = 0
        self.lastImageCountL = 0

        # Last meassured value, to inform GCS
        self.lastCpuTemp = 0
        self.lastBoxTemp = 0
        self.lastHumidity = 0

        rospy.loginfo("Started Inflight Watchdog.")

        # Publisher
        self.diagStringsPub = rospy.Publisher(
            "/mavros/statustext/send", StatusText, queue_size=10
        )
        self.imageCountLPublisher = rospy.Publisher(
            "/searchwing/imagecount/caml", Int16, queue_size=10
        )
        self.imageCountRPublisher = rospy.Publisher(
            "/searchwing/imagecount/camr", Int16, queue_size=10
        )
        self.debugValuePublisher = rospy.Publisher(
            "/mavros/debug_value/send", DebugValue, queue_size=10
        )

        # Subscriber
        self.droneStateSubscriber = rospy.Subscriber(
            "/searchwing/state", DroneState, self.droneStateWatchdog
        )
        self.humiditySubscriber = rospy.Subscriber(
            "/box/humidity", Float64, self.humidityWatchdog
        )
        self.boxTempSubscriber = rospy.Subscriber(
            "/box/temperature",
            Float64,
            self.boxTemperatureWatchdog,
        )
        self.cpuTemperatureSubscriber = rospy.Subscriber(
            "/cpu/temperature", Float64, self.cpuTemperatureWatchdog
        )

    """
    Check if connection to autopilot is established.
    Start image counting.
    Start Watchdog timer/routines.
    """

    def start(self):

        rospy.loginfo("Waiting for autopilot connection.")
        while watchdog.droneStateMessage is None:
            rospy.sleep(2)

        # Inform QGroundcontrol of InflightWatchog start
        msg = StatusText()
        msg.header = watchdog.droneStateMessage.header
        msg.severity = StatusText.NOTICE
        msg.text = "Inflight watchdog started. Woof."
        watchdog.diagStringsPub.publish(msg)
        msg.text = "Waiting for liftoff. "
        watchdog.diagStringsPub.publish(msg)

        # wait for liftoff
        rospy.loginfo("Waiting for liftoff.")
        while not watchdog.droneStateMessage.isFlying.data:
            rospy.sleep(2)

        rospy.loginfo("Liftoff!")

        # count images every period
        self.imageCounterTimer = rospy.Timer(
            rospy.Duration(self.imageCountInterval), watchdog.countImages
        )

        # check if images are taken
        self.imageCheckTimer = rospy.Timer(
            rospy.Duration(self.imageCountWatchdogInterval), watchdog.imageCountWatchdog
        )

        # send debug values
        self.debugValueTimer = rospy.Timer(
            rospy.Duration(self.sendDebugValueInterval), watchdog.sendDebugValues
        )

        # inform every period QGRoundcontrol about drone state
        self.infoTimer = rospy.Timer(
            rospy.Duration(self.informGCSInterval), watchdog.informGroundControl
        )

    """
    Stop Watchdogs / timers
    """

    def stop(self):
        rospy.loginfo("Stopping Watchdog.")
        self.infoTimer.shutdown()
        self.imageCheckTimer.shutdown()
        self.imageCounterTimer.shutdown()
        self.debugValueTimer.shutdown()

    """
    Process current drone/mission status.
    Detects if new mission was started.
    """

    def droneStateWatchdog(self, stateMessage):
        self.droneStateMessage = stateMessage

        if stateMessage.missionName.data not in self.missionName:  # detect new Mission
            rospy.loginfo(
                "Inflight watchdog detected new mission, resetting image count."
            )
            self.currentImageCountL = 0
            self.currentImageCountR = 0

        self.missionName = stateMessage.missionName.data

    """
    Counts images of each camera.
    """

    def countImages(self, timerEvent):

        if self.missionName == "":
            rospy.loginfo("No mission name found. Aborting image count.")
            return

        # Count images - L
        try:
            path = os.path.join("/data/bilder/", self.missionName, "cam-l")
            self.currentImageCountL = len(listdir(path))
        except FileNotFoundError:
            rospy.loginfo(
                "cam-l folder was not created. Is the camera attached / working?"
            )
        finally:
            self.imageCountLPublisher.publish(self.currentImageCountL)

        # Count images - R
        try:
            path = os.path.join("/data/bilder/", self.missionName, "cam-r")
            self.currentImageCountR = len(listdir(path))
        except FileNotFoundError:
            rospy.loginfo(
                "cam-r folder was not created. Is the camera attached / working?"
            )
        finally:
            self.imageCountRPublisher.publish(self.currentImageCountR)

    """
    Checkoing Image count in extra watchdog. Allows adjustment of user information period.
    """

    def imageCountWatchdog(self, timerEvent):
        if self.droneStateMessage is None:
            rospy.loginfo("No drone State detectd, cancelling image count Watchdog.")
            return

        # Check left Image Count
        if (
            self.lastImageCountL == self.currentImageCountL
            or self.currentImageCountL == 0
        ):
            # No images taken
            rospy.loginfo("No images are taken on cam-l.")
            msg = StatusText()
            msg.header = self.droneStateMessage.header
            msg.severity = (
                StatusText.ALERT
            )  # "Action should be taken immediately. Indicates error in non-critical systems."
            msg.text = "No images are taken on cam-l."
            self.diagStringsPub.publish(msg)
            msg.text = "Currently {:04d} images were saved on cam-l".format(
                self.currentImageCountL
            )
            self.diagStringsPub.publish(msg)

        if (
            self.lastImageCountR == self.currentImageCountR
            or self.currentImageCountR == 0
        ):
            # No images taken
            rospy.loginfo("No images are taken on cam-r.")
            msg = StatusText()
            msg.header = self.droneStateMessage.header
            msg.severity = (
                StatusText.ALERT
            )  # "Action should be taken immediately. Indicates error in non-critical systems."
            msg.text = "No images are taken on cam-r."
            self.diagStringsPub.publish(msg)
            msg.text = "Currently {:04d} images were saved".format(
                self.currentImageCountR
            )
            self.diagStringsPub.publish(msg)

        self.lastImageCountL = self.currentImageCountL
        self.lastImageCountR = self.currentImageCountR

    """
    Humidity Watchdog. Checks humidity values and warns user if above threshold.
    """

    def humidityWatchdog(self, value: Float64):

        if self.droneStateMessage is None:
            rospy.loginfo("No drone State detectd, cancelling humidity Watchdog.")
            return

        if value.data >= self.humidityThreshold:
            msg = StatusText()
            msg.header = self.droneStateMessage.header
            msg.severity = (
                StatusText.CRITICAL
            )  # "Action must be taken immediately. Indicates failure in a primary system."
            msg.text = (
                "Critical Humidity: "
                + "{:.1f}".format(value.data)
                + " > "
                + "{:.1f}%".format(self.humidityThreshold)
                + ". You should immediately power down the UAV."
            )
            self.diagStringsPub.publish(msg)

        self.lastHumidity = value.data

    """
    Internal temperature Watchdog. Checks temperature values and warns user if above threshold.
    """

    def boxTemperatureWatchdog(self, value: Float64):

        if self.droneStateMessage is None:
            rospy.loginfo("No drone State detectd, cancelling humidity Watchdog.")
            return

        if value.data >= self.boxTempThreshold:
            msg = StatusText()
            msg.header = self.droneStateMessage.header
            msg.severity = (
                StatusText.ALERT
            )  # "Action should be taken immediately. Indicates error in non-critical systems."
            msg.text = (
                "Critical Box Temperature: "
                + "{:.1f}".format(value.data)
                + " > "
                + "{:.1f}°C".format(self.cpuTempThreshold)
            )
            self.diagStringsPub.publish(msg)

        self.lastBoxTemp = value.data

    """
    CPU temperature Watchdog. Checks temperature values and warns user if above threshold.
    """

    def cpuTemperatureWatchdog(self, value: Float64):

        if self.droneStateMessage is None:
            rospy.loginfo("No drone State detectd, cancelling humidity Watchdog.")
            return

        if value.data >= self.cpuTempThreshold:
            msg = StatusText()
            msg.header = self.droneStateMessage.header
            msg.severity = (
                StatusText.ALERT
            )  # "Action should be taken immediately. Indicates error in non-critical systems."
            msg.text = (
                "Critical CPU Temperature: "
                + "{:.1f}".format(value.data)
                + " > "
                + "{:.1f}°C".format(self.cpuTempThreshold)
            )
            self.diagStringsPub.publish(msg)

        self.lastCpuTemp = value.data

    """
    Send Debug values.
    Extra method in order to control mavlink trafic.
    """

    def sendDebugValues(self, timerEvent):
        # Message Header
        debugValue = DebugValue()
        debugValue.header = self.droneStateMessage.header

        debugValue.name = "cpu/temp"  # only 10 char
        debugValue.type = DebugValue.TYPE_NAMED_VALUE_FLOAT
        debugValue.value_float = self.lastCpuTemp
        self.debugValuePublisher.publish(debugValue)

        debugValue.name = "box/temp"  # only 10 char
        debugValue.type = DebugValue.TYPE_NAMED_VALUE_FLOAT
        debugValue.value_float = self.lastBoxTemp
        self.debugValuePublisher.publish(debugValue)

        debugValue.name = "box/hum"  # only 10 char
        debugValue.type = DebugValue.TYPE_NAMED_VALUE_FLOAT
        debugValue.value_float = self.lastHumidity
        self.debugValuePublisher.publish(debugValue)

        debugValue.name = "images/l"  # only 10 char
        debugValue.type = DebugValue.TYPE_NAMED_VALUE_INT
        debugValue.value_int = self.currentImageCountL
        self.debugValuePublisher.publish(debugValue)

        debugValue.name = "images/r"  # only 10 char
        debugValue.type = DebugValue.TYPE_NAMED_VALUE_INT
        debugValue.value_int = self.currentImageCountR
        self.debugValuePublisher.publish(debugValue)

    """
    Informs GroundControlStation about payload/drone state.
    """

    def informGroundControl(self, timerEvent):
        rospy.loginfo("Sending info")
        msg = StatusText()
        msg.header = self.droneStateMessage.header
        msg.severity = StatusText.INFO
        # Humidity
        msg.text = "Box humidity: {:.1f}%".format(self.lastHumidity)
        self.diagStringsPub.publish(msg)
        # Box Temperature
        msg.text = "Box temperature: {:.1f}°C".format(self.lastBoxTemp)
        self.diagStringsPub.publish(msg)
        # CPU Temperature
        msg.text = "CPU temperature: {:.1f}°C".format(self.lastCpuTemp)
        self.diagStringsPub.publish(msg)
        # Image count
        msg.text = "Cam-L | Cam-R"
        self.diagStringsPub.publish(msg)
        msg.text = (
            "{:04d}".format(self.currentImageCountL)
            + " | "
            + "{:04d}".format(self.currentImageCountR)
        )
        self.diagStringsPub.publish(msg)


watchdog = InflightWatchdog()

watchdog.start()

rospy.spin()

watchdog.stop()
