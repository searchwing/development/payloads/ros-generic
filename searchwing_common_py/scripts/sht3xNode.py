#!/usr/bin/env python

import os

import rospy
from std_msgs.msg import Float64

upperIntervalBoundary = 300.0
lowerIntervalBoundary = 0.1

rospy.init_node("ShtNode")

tempSensor = "/sys/class/hwmon/hwmon2/temp1_input"
humSensor = "/sys/class/hwmon/hwmon2/humidity1_input"

if not os.path.exists(tempSensor):
    rospy.logerr("Temperature-sensor doesnt exist:{}".format(tempSensor))
    rospy.logerr("Exit...")
    exit(0)
if not os.path.exists(humSensor):
    rospy.logerr("Humidity-sensor doesnt exist:{}".format(humSensor))
    rospy.logerr("Exit...")
    exit(0)

readSensorInterval = float(rospy.get_param("~readSensorInterval", 4.0))
if (
    readSensorInterval < lowerIntervalBoundary
    or readSensorInterval > upperIntervalBoundary
):
    rospy.loginfo(
        "Sensor read interval out of bounds. Should be between {:.3f} and {:.0f} s. Using default value: {:.1f} s.".format(
            lowerIntervalBoundary, upperIntervalBoundary, 4.0
        )
    )
    readSensorInterval = 4.0

shtTempPub = rospy.Publisher("/box/temperature", Float64, queue_size=2)
shtHumPub = rospy.Publisher("/box/humidity", Float64, queue_size=2)


def readShtTemp():
    with open("/sys/class/hwmon/hwmon2/temp1_input") as file:
        temp = int(file.read()) / 1e3
        shtTempPub.publish(temp)


def readShtHumidity():
    with open("/sys/class/hwmon/hwmon2/humidity1_input") as file:
        hum = int(file.read()) / 1e3
        shtHumPub.publish(hum)


rate = rospy.Rate(1 / readSensorInterval)
while not rospy.is_shutdown():
    readShtTemp()
    readShtHumidity()
    rate.sleep()

rospy.spin()
