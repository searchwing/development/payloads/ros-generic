#!/usr/bin/env python

import rospy
from std_msgs.msg import Float64

rospy.init_node("CpuTempNode")

cpuTempPub = rospy.Publisher("/cpu/temperature", Float64, queue_size=2)


def readCpuTemp():
    temp = int(open("/sys/class/thermal/thermal_zone0/temp").read()) / 1e3
    cpuTempPub.publish(temp)


while not rospy.is_shutdown():
    rate = rospy.Rate(0.5)
    readCpuTemp()
    rate.sleep()
