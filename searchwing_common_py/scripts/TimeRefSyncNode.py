#!/usr/bin/env python

import datetime
import os

import message_filters
import rospy
from pytz import timezone
from sensor_msgs.msg import NavSatFix, TimeReference
from timezonefinder import TimezoneFinder

tf = TimezoneFinder()


def callback(timeReferenceMsg, gpsPosMsg):

    stamp_sec = timeReferenceMsg.time_ref.to_sec()
    if stamp_sec < 1649183514:  # Tuesday, April 5, 2022 8:31:54 PM GMT+02:00 DST
        rospy.logwarn(
            "Received timeref msg with stamp {} seems to be invalid".format(str(stamp))
        )
        return

    # reformat timestamp
    stamp = datetime.datetime.fromtimestamp(stamp_sec)
    stamp_string = stamp.strftime("%Y-%m-%d %H:%M:%S")

    # find timezone for given gps pos
    tz = timezone(
        tf.certain_timezone_at(lng=gpsPosMsg.longitude, lat=gpsPosMsg.latitude)
    )

    # set os time
    setdatecmd = "timedatectl set-time '{}'".format(str(stamp))
    returned_value = os.system(setdatecmd)
    if returned_value != 0:
        rospy.logwarn(
            "Error {} while trying to set OS clock: {}".format(
                returned_value, setdatecmd
            )
        )
        return 0
    rospy.loginfo("Succesfully set OS clock: {}".format(setdatecmd))

    # set os timezone
    setdatecmd = "timedatectl set-timezone {}".format(str(tz))
    print(setdatecmd)
    returned_value = os.system(setdatecmd)
    if returned_value != 0:
        rospy.logwarn(
            "Error {} while trying to set OS clock: {}".format(
                returned_value, setdatecmd
            )
        )
        return
    rospy.loginfo("Succesfully set OS timezone: {}".format(setdatecmd))

    # unregister subscribers when done
    timeReferenceTopic_sub.unregister()
    gpsPosTopic_sub.unregister()


rospy.init_node("TimeRefSyncNode")
timeReferenceTopicName = rospy.get_param("~time_ref_topic", "/mavros/time_reference")
gpsPosTopicName = rospy.get_param("~gps_pos_topic", "/mavros/global_position/global")

global timeReferenceTopic_sub
timeReferenceTopic_sub = message_filters.Subscriber(
    timeReferenceTopicName, TimeReference
)
global gpsPosTopic_sub
gpsPosTopic_sub = message_filters.Subscriber(gpsPosTopicName, NavSatFix)

synced_data = message_filters.ApproximateTimeSynchronizer(
    [timeReferenceTopic_sub, gpsPosTopic_sub], 10, slop=1
)  # "slop" parameter in the constructor that defines the delay (in seconds) with which messages can be synchronized

synced_data.registerCallback(callback)

rospy.spin()
