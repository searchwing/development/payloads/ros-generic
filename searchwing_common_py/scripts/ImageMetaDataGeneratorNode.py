#!/usr/bin/env python

import datetime
import threading
from socket import gethostname

import numpy as np
import rospy
import tf
from geometry_msgs.msg import PointStamped, PoseStamped
from sensor_msgs.msg import CameraInfo, NavSatFix
from std_msgs.msg import Bool, Float64, String

from searchwing_common_py import camCalibProvider
from searchwing_msgs.msg import DroneState, GpsPos, ImageMetadata

# start node
rospy.init_node("ImageMetaDataGeneratorNode")

from searchwing_common_py.imagePointTransformations import (
    getPixelGPSPosOnWater,
    getPixelGSD,
)

tf_listener = tf.TransformListener()  # must be outside callback

camTopicBaseName = rospy.get_param("~i_camName", "/camr")

# cam calib
cameraTfName = rospy.get_param("~i_cameraTfName", "camr")
camCalib = None

gpsLock = threading.Lock()
compassLock = threading.Lock()
droneStateMsgLock = threading.Lock()

gpsPos = None
compassHeading = None
droneStateMsg = None


def callbackSearchwingState(stateMsg):
    global droneStateMsg
    droneStateMsgLock.acquire()
    droneStateMsg = stateMsg
    droneStateMsgLock.release()


def callbackAPMCompass(compassHeadingMsg):
    global compassHeading
    compassLock.acquire()
    compassHeading = compassHeadingMsg
    compassLock.release()


def callbackAPMGPS(gpsPosMsg):
    global gpsPos
    gpsLock.acquire()
    gpsPos = gpsPosMsg
    gpsLock.release()
    # TODO fix altitude to get ASML alt
    # https://wiki.ros.org/mavros#mavros.2FPlugins.Avoiding_Pitfalls_Related_to_Ellipsoid_Height_and_Height_Above_Mean_Sea_Level


def callbackCamInfo(camInfoMsg):
    global gpsPos
    global compassHeading
    global droneStateMsg
    global camCalib

    localGpsPos = None
    localCompassHeading = None
    localDroneStateMsg = None

    droneStateMsgLock.acquire()
    localDroneStateMsg = droneStateMsg
    droneStateMsgLock.release()

    compassLock.acquire()
    localCompassHeading = compassHeading
    compassLock.release()

    gpsLock.acquire()
    localGpsPos = gpsPos
    gpsLock.release()

    if localDroneStateMsg is None:
        localDroneStateMsg = DroneState()
        localDroneStateMsg.isFlying = Bool(True)
        localDroneStateMsg.missionName = String("noMission-" + gethostname())

    if camCalib is None:
        camCalib = camCalibProvider.getCameraModelFromCameraInfoMsg(camInfoMsg)

    imageMetadataMsg = ImageMetadata()
    imageMetadataMsg.header = camInfoMsg.header
    imageMetadataMsg.sensorName = String(cameraTfName)
    if localDroneStateMsg.isFlying.data == False:
        imageMetadataMsg.imageName = String("preflight.jpg")
    else:
        imageMetadataMsg.imageName = String(
            datetime.datetime.fromtimestamp(
                rospy.Time.to_time(camInfoMsg.header.stamp)
            ).strftime("%Y-%m-%dT%H-%M-%S.%f")
            + ".jpg"
        )

    imageMetadataMsg.imageCornerGpsPos = [GpsPos()] * 5  # Fill with empty positions
    imageMetadataMsg.imageCornerGSD = [0.0] * 5  # Fill with empty GSD
    if not localGpsPos is None:
        stamp = camInfoMsg.header.stamp
        valid = True
        # Get 3D-Position of drone in local NED coordinates
        try:
            tf_listener.waitForTransform(
                "map",
                cameraTfName,
                stamp,
                rospy.Duration(
                    1.5
                ),  # wait SHORTER than image periode time otherwise the wait accumulates
            )
        except:
            rospy.logwarn(
                "Timeout tf {}->map. Cant calc detailed imagemetadata for stamp {}".format(
                    cameraTfName, str(stamp)
                )
            )
            valid = False
        if valid:
            drone3dPose = PoseStamped()
            drone3dPose.header.frame_id = "base_link"
            drone3dPose.header.stamp = stamp
            drone3dPoseInMap = tf_listener.transformPose("map", drone3dPose)
            drone3dPos = PointStamped()
            drone3dPos.header = drone3dPose.header
            drone3dPos.point = drone3dPoseInMap.pose.position

            fillImagePixelMetadata(
                camInfoMsg, localGpsPos, drone3dPos, stamp, imageMetadataMsg
            )  # Fill list
            imageMetadataMsg.droneGpsPos = localGpsPos
            imageMetadataMsg.droneLocalPose = drone3dPoseInMap.pose
    if not localDroneStateMsg is None:
        imageMetadataMsg.droneState = localDroneStateMsg
    if not localCompassHeading is None:
        imageMetadataMsg.heading = localCompassHeading.data

    imageMetadataPub.publish(imageMetadataMsg)


def fillImagePixelMetadata(
    camInfoMsg, localGpsPos, drone3dPosInMap, stamp, imageMetadataMsg
):

    for imgPosIdx in range(5):
        if imgPosIdx == ImageMetadata.GPS_POS_IDX_UL:
            pt2D = np.array([0, 0], int)
        elif imgPosIdx == ImageMetadata.GPS_POS_IDX_UR:
            pt2D = np.array([camInfoMsg.width, 0], int)
        elif imgPosIdx == ImageMetadata.GPS_POS_IDX_LR:
            pt2D = np.array([camInfoMsg.width, camInfoMsg.height], int)
        elif imgPosIdx == ImageMetadata.GPS_POS_IDX_LL:
            pt2D = np.array([0, camInfoMsg.height], int)
        elif imgPosIdx == ImageMetadata.GPS_POS_IDX_CE:
            pt2D = np.array([camInfoMsg.width / 2.0, camInfoMsg.height / 2.0], int)
        else:
            return

        ptGPS, valid = getPixelGPSPosOnWater(
            pt2D, drone3dPosInMap, localGpsPos, cameraTfName, camCalib, stamp
        )
        imageMetadataMsg.imageCornerGpsPos[imgPosIdx] = GpsPos()
        if valid:
            imageMetadataMsg.imageCornerGpsPos[imgPosIdx].lat = ptGPS[0]
            imageMetadataMsg.imageCornerGpsPos[imgPosIdx].lon = ptGPS[1]
            imageMetadataMsg.imageCornerGpsPos[imgPosIdx].alt = ptGPS[2]
        else:
            rospy.logwarn(
                "Invalid GPS Position calculated maybe due to pixel above horizon"
            )
            imageMetadataMsg.imageCornerGpsPos[imgPosIdx].lat = np.nan
            imageMetadataMsg.imageCornerGpsPos[imgPosIdx].lon = np.nan
            imageMetadataMsg.imageCornerGpsPos[imgPosIdx].alt = np.nan

        ptGSD, valid = getPixelGSD(
            pt2D, drone3dPosInMap, localGpsPos, cameraTfName, camCalib, stamp
        )
        if valid:
            imageMetadataMsg.imageCornerGSD[imgPosIdx] = ptGSD
        else:
            rospy.logwarn(
                "Invalid GroundSamplingDistance calculated maybe due to pixel above horizon"
            )
            imageMetadataMsg.imageCornerGSD[imgPosIdx] = np.nan


# subscriber
rospy.Subscriber(camTopicBaseName + "/camera_info", CameraInfo, callbackCamInfo)
rospy.Subscriber("/mavros/global_position/compass_hdg", Float64, callbackAPMCompass)
rospy.Subscriber("/mavros/global_position/global", NavSatFix, callbackAPMGPS)
rospy.Subscriber("/searchwing/state", DroneState, callbackSearchwingState)

# publisher
imageMetadataPub = rospy.Publisher(
    camTopicBaseName + "/imageMetadata", ImageMetadata, queue_size=2
)


rospy.spin()
