#!/usr/bin/env python
import datetime
import os
import subprocess
import threading
import time

import numpy as np
import rospy
from image_priorization.queue_greedy_priorization import PrioretizedQueueGreedy
from image_priorization.structures import Detection, ImageWithDetections, PointRest

from searchwing_msgs.msg import ImageMetadata

rsync_error_codes = {
    0: "Success",
    1: "Syntax or usage error",
    2: "Protocol incompatibility",
    3: "Errors selecting input/output files, dirs",
    4: "Requested action not supported",
    5: "Error starting client-server protocol",
    6: "Daemon unable to append to log-file",
    10: "Error in socket I/O",
    11: "Error in file I/O",
    12: "Error in rsync protocol data stream",
    13: "Errors with program diagnostics",
    14: "Error in IPC code",
    20: "Received SIGUSR1 or SIGINT",
    21: "Some error returned by waitpid()",
    22: "Error allocating core memory buffers",
    23: "Partial transfer due to error",
    24: "Partial transfer due to vanished source files",
    25: "The --max-delete limit stopped deletions",
    30: "Timeout in data send/receive",
    35: "Timeout waiting for daemon connection",
}


class ImageSenderNode:
    def __init__(self) -> None:

        rospy.init_node("ImageSenderNode")

        self.rsyncReceiverURL = rospy.get_param(
            "~rsyncReceiverURL", "rsync://192.168.178.21:873/flightdata/"
        )
        self.metaTopicNameL = rospy.get_param("~metaTopic", "/caml/imageMetadata")
        self.metaTopicNameR = rospy.get_param("~metaTopic", "/camr/imageMetadata")
        self.imageFolder = rospy.get_param("~imageFolder", "/data/bilder")
        self.mode = rospy.get_param(
            "~mode", "sendPrioritized"
        )  # sendLatest sendPrioritized

        self.all_images = []

        if self.mode == "sendPrioritized":        
            self.mode_prio_queue = PrioretizedQueueGreedy()

        # subscriber
        self.metaSubR = rospy.Subscriber(
            name=self.metaTopicNameL, data_class=ImageMetadata, callback=self.metaSub_cb, callback_args=self.metaTopicNameL
        )
        self.metaSubL = rospy.Subscriber(
            name=self.metaTopicNameR, data_class=ImageMetadata, callback=self.metaSub_cb, callback_args=self.metaTopicNameR
        )

        # sender
        self.sending_thread = threading.Thread(target=self.send_images)
        self.sending_thread.start()

        self.print_queue_info_timer = rospy.Timer(
            rospy.Duration(20), self.print_queue_info
        )

    def metaSub_cb(self, msg, args):
        if "latest.jpg" in msg.imageName.data or "preflight" in msg.imageName.data:
            return

        if self.mode == "sendLatest":
            self.all_images.append(msg)
        elif self.mode == "sendPrioritized":

            def gpsToPointRest(gpsPos):
                return PointRest(lat=gpsPos.lat, lon=gpsPos.lon)

            def invalidGps(gpsPos):
                if np.isnan(gpsPos.lat) or np.isnan(gpsPos.lon):
                    return True
                else:
                    return False

            polygon_points = []
            for i in range(4):
                gpsPos = msg.imageCornerGpsPos[i]
                if invalidGps(gpsPos):
                    return
                else:
                    polygon_points.append(gpsToPointRest(gpsPos))
            detections = Detection()
            image_name = msg.imageName.data
            image_name = image_name.replace(".jpg", "")
            datetime_original = datetime.datetime.strptime(
                image_name, "%Y-%m-%dT%H-%M-%S.%f"
            )
            new_image = ImageWithDetections(
                image_area=polygon_points,
                detections=detections,
                datetime_original=datetime_original,
            )
            self.mode_prio_queue.add_image(new_image)

            self.all_images.append(msg)  # add after prio queue to ensure queue is ready
        else:
            exit()

    def execute_image_transmission(self, imageToSendMsg):
        if imageToSendMsg.sensorName.data == "caml":
            camFolder = "cam-l"
        else:
            camFolder = "cam-r"

        time.sleep(
            0.2
        )  # TODO find other way to wait until file write is finished to disk

        fileToSend = os.path.join(
            self.imageFolder,
            imageToSendMsg.droneState.missionName.data,
            camFolder,
            imageToSendMsg.imageName.data,
        )
        if not os.path.exists(fileToSend):
            rospy.logerr(
                "{}: Not saved on Disk. Abort transmission.".format(fileToSend)
            )
            return 11
        
        targetPath = str(fileToSend).replace(self.imageFolder, "")[1:]

        rsync_cmd = [
            "rsync",
            "--timeout=3",
            "--mkpath",
            "-q",
            "--partial",
            "--whole-file",
            fileToSend,
            "{}{}".format(self.rsyncReceiverURL, targetPath),
        ]
        rsync_cmd_dbg = " ".join(rsync_cmd)
        rospy.logdebug("{}".format(rsync_cmd_dbg))

        ret = subprocess.run(rsync_cmd)  # execute rsync of file

        self.log_returncode(ret.returncode, imageToSendMsg)
        return ret.returncode

    def send_next_image(self):
        queueLen = len(self.all_images)
        if queueLen > 0:
            if self.mode == "sendLatest":
                imageToSend = self.all_images[-1]

                imageToSendIdx = self.all_images.index(imageToSend)
                imageToSendText = imageToSend.sensorName.data+"/"+imageToSend.imageName.data
                rospy.loginfo(
                    "{}: Send as #{}:...".format(imageToSend.imageName.data,imageToSendIdx)
                )
                ret = self.execute_image_transmission(imageToSend)
                if ret == 0:
                    self.all_images.remove(imageToSend)

            elif self.mode == "sendPrioritized":
                highest_prio_image = self.mode_prio_queue.get_image_highest_prio()
                if highest_prio_image is None:
                    rospy.loginfo("Image-prioretized-queue empty...")
                    time.sleep(0.5)
                    return
                
                highest_prio_image_datetime = highest_prio_image.datetime_original
                highest_prio_image_filename = (
                    highest_prio_image_datetime.strftime("%Y-%m-%dT%H-%M-%S.%f")
                    + ".jpg"
                )
                imageToSend = None
                for image in self.all_images:
                    if image.imageName.data == highest_prio_image_filename:
                        imageToSend = image
                        break
                if imageToSend is None:
                    rospy.logerr(
                        "{}: Cant find image".format(highest_prio_image_filename)
                    )
                    self.mode_prio_queue.set_image_sent(highest_prio_image)
                    return

                imageToSendText = imageToSend.sensorName.data+"/"+imageToSend.imageName.data
                rospy.loginfo("{}: Prio {}".format(imageToSendText, highest_prio_image['geom_prio']))
                rospy.loginfo(
                    "{}: Send as #{}:...".format(imageToSendText,0)
                )

                ret = self.execute_image_transmission(imageToSend)
                if ret == 0 or ret == 11:
                    self.all_images.remove(imageToSend)
                    self.mode_prio_queue.set_image_sent(highest_prio_image)

            else:
                exit()

    def send_images(self):
        while True:
            self.send_next_image()

    def log_returncode(self, code, imageToSendMsg):
        imgName = imageToSendMsg.imageName.data
        imageToSendText = imageToSendMsg.sensorName.data+"/"+imageToSendMsg.imageName.data
        if code == 0:
            rospy.loginfo("{}: {}".format(imageToSendText, rsync_error_codes[code]))
        else:
            rospy.logerr("{}: {}".format(imageToSendText, rsync_error_codes[code]))

    def print_queue_info(self, dummy):
        if self.mode == "sendLatest":
            queueLen = len(self.all_images)
            rospy.loginfo("Image-input-queuelen: {}".format(queueLen))

        elif self.mode == "sendPrioritized":
            queueLen = len(self.all_images)
            rospy.loginfo("Image-input-queuelen: {}".format(queueLen))
            queue = self.mode_prio_queue.get_queue()
            queueLen = len(queue[queue['sent']==False])
            rospy.loginfo("Image-prio-queuelen: {}".format(queueLen))

sender = ImageSenderNode()

rospy.spin()
