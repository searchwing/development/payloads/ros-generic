#!/usr/bin/env python

import io
import os
from glob import glob

import piexif
import rospy
from sensor_msgs.msg import CompressedImage

from searchwing_msgs.msg import ImageMetadata


def callbackImagemetadata(msg):
    global allImages
    global removeExif
    imageFilename = msg.imageName.data
    foundImages = [x for x in allImages if imageFilename in x]
    if len(foundImages) == 0:
        return
    imagePath = foundImages[0]
    binaryPicFile = open(imagePath, "rb")
    binaryPic = binaryPicFile.read()
    if removeExif:
        binaryPicWithoutExif = io.BytesIO()
        piexif.remove(binaryPic, binaryPicWithoutExif)
        binaryPic = binaryPicWithoutExif.getvalue()

    picMsg = CompressedImage()
    picMsg.header = msg.header
    picMsg.format = "jpg"
    picMsg.data = binaryPic

    imagesPub.publish(picMsg)


flightdataPath = os.getenv("SW_FLIGHTDATA_PATH")
assert (
    not flightdataPath is None
), "define SW_FLIGHTDATA_PATH - export SW_FLIGHTDATA_PATH=/path/to/data"

# start node
rospy.init_node("ImageFlightReplayPublisherNode", anonymous=True)

metadataTopicName = rospy.get_param("~i_imgTopic", "/caml/imageMetadata")
removeExif = rospy.get_param("~i_removeExif", True)
imageTopicName = rospy.get_param("~o_tracksTopicName", "/caml/image/compressed")


allImages = glob(os.path.join(flightdataPath, "**/*.jpg"), recursive=True)


rospy.Subscriber(metadataTopicName, ImageMetadata, callbackImagemetadata)
imagesPub = rospy.Publisher(imageTopicName, CompressedImage, queue_size=2)


rospy.spin()
