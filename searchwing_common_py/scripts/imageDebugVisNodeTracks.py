#!/usr/bin/env python

import cv2

cv2.useOptimized()

import message_filters
import rospy
from sensor_msgs.msg import CompressedImage, Image

from searchwing_common_py import camCalibProvider
from searchwing_msgs.msg import TrackArray

# start node
rospy.init_node("imageDebugVisNodeTracks", anonymous=True)

from searchwing_common_py.imageDebugVis import (  # needs to be after init for tf_listener in debugVis
    imageDebugVis,
)

stampSyncStrategy = rospy.get_param("~p_stampSyncStrategy", "approxStamp")

imgTopicName = rospy.get_param("~i_imgTopic", "/camera/image/compressed")
tracksTopicName = rospy.get_param("~i_tracksTopicName", "/tracks")

# cam calib
cameraTfName = rospy.get_param("~i_cameraTfName", "cam")
cameraCalibFilePath = rospy.get_param(
    "~i_cameraCalibFilePath",
    "catkin_ws/src/ros-generic/searchwing_common_py/config/frankiCamCalib.yaml",
)
camCalib = camCalibProvider.getCameraModel(cameraCalibFilePath)

if tracksTopicName == "":
    rospy.logerr("please define a topicname to visualize")
    exit

tracksMsg = None
imgMsg = None


def callbackTracks(msg):
    global imgMsg
    global tracksMsg
    tracksMsg = msg
    if not tracksMsg is None and not imgMsg is None:
        debugVisTracks.callbackTracks(imgMsg, tracksMsg)


def callbackImg(msg):
    global tracksMsg
    global imgMsg
    imgMsg = msg
    if not tracksMsg is None and not imgMsg is None:
        debugVisTracks.callbackTracks(imgMsg, tracksMsg)


# subscriber
if tracksTopicName != "":
    debugImagetracks_pub = rospy.Publisher("/debugImageTracks", Image, queue_size=2)
    debugVisTracks = imageDebugVis(debugImagetracks_pub, camCalib, "cam")

    if stampSyncStrategy == "equalStamp":
        image_sub = message_filters.Subscriber(imgTopicName, CompressedImage)
        tracks_sub = message_filters.Subscriber(tracksTopicName, TrackArray)

        synced_tracks = message_filters.TimeSynchronizer([image_sub, tracks_sub], 3)
        synced_tracks.registerCallback(debugVisTracks.callbackTracks)
    elif stampSyncStrategy == "approxStamp":

        rospy.Subscriber(imgTopicName, CompressedImage, callbackImg)
        rospy.Subscriber(tracksTopicName, TrackArray, callbackTracks)

    else:
        rospy.logerr("unsupported stampSyncStrategy chosen")
        exit


print("imageDebugVisNodeTracks started: Loop until new data arrives")
rospy.spin()
