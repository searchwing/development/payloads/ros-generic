#!/usr/bin/env python

import cv2

cv2.useOptimized()
import numpy as np

# ros stuff
import rospy
import tf
from geometry_msgs.msg import Point, PointStamped
from vision_msgs.msg import Detection2DArray

from searchwing_common_py import camCalibProvider
from searchwing_msgs.msg import Detection3D, Detection3DArray

# start node
rospy.init_node("BBoxToGroundProjectionNode")

from searchwing_common_py import imagePointTransformations

tf_listener = tf.TransformListener()  # must be outside callback


def callbackDetections2D(Detections2D):

    detectionsStamp = Detections2D.header.stamp

    # Get 3D-Position of drone in the World
    drone3dPos = PointStamped()
    drone3dPos.point.x = 0
    drone3dPos.point.y = 0
    drone3dPos.point.z = 0
    drone3dPos.header.frame_id = "base_link"
    drone3dPos.header.stamp = detectionsStamp
    tf_listener.waitForTransform(
        "base_link", "map", detectionsStamp, rospy.Duration(3)
    )  # wait until the needed transformation is ready, as the image can be provided faster than the telemetry
    drone3dPosInMap = tf_listener.transformPoint("map", drone3dPos)

    # convert 2d => 3d
    detections3D = Detection3DArray()
    for idx, one2DDet in enumerate(Detections2D.detections):
        pt2D = np.array([one2DDet.bbox.center.x, one2DDet.bbox.center.y], int)
        pt3D, valid = imagePointTransformations.getPixel3DPosOnWater(
            pt2D, drone3dPosInMap, cameraTfName, camCalib, detectionsStamp
        )  # project pixel to ground
        if not valid:
            rospy.logwarn(
                "Invalid 3D position calculated maybe due to pixel above horizon. Skip drawing of object"
                + str(idx)
                + " in camera "
                + cameraTfName
            )
        else:
            detection3D = Detection3D()
            detection3D.position = Point()
            detection3D.position.x = pt3D[0]
            detection3D.position.y = pt3D[1]
            detection3D.position.z = pt3D[2]
            detection3D.DetectionArrayIdx = idx

            detections3D.detections.append(detection3D)

    detections3D.header.stamp = detectionsStamp
    Detections3DPub.publish(detections3D)


# cam calib
cameraTfName = rospy.get_param("~i_cameraTfName", "thermal")
cameraCalibFilePath = rospy.get_param("~i_cameraCalibFilePath", "")
camCalib = camCalibProvider.getCameraModel(cameraCalibFilePath)

# subscriber
detections2DTopicName = rospy.get_param(
    "~i_detections2DTopicName", "/detections/Thermal"
)
rospy.Subscriber(detections2DTopicName, Detection2DArray, callbackDetections2D)

# publisher
detections3DTopicName = rospy.get_param(
    "~o_detections3DTopicName", "/detections/Detections3D"
)
Detections3DPub = rospy.Publisher(detections3DTopicName, Detection3DArray, queue_size=2)

rospy.spin()
