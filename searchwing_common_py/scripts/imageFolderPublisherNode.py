#!/usr/bin/env python


import cv2

cv2.useOptimized()
import os
import time
from datetime import datetime
from glob import glob

# ros stuff
import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import Image


def imageFolderPublisherNode():
    # start node
    rospy.init_node("imageFolderPublisherNode", anonymous=True)

    imgTopicName = rospy.get_param("~imgTopic", "/camera/image")
    imageFolder = rospy.get_param("~imageFolder")
    path = imageFolder
    # publisher
    imgStampFormat = rospy.get_param("~imgStampFormat", "%Y-%m-%dT%H:%M:%S.%f")
    imagesPub = rospy.Publisher(imgTopicName, Image, queue_size=2)

    use_sim_time = rospy.get_param("use_sim_time", False)

    images = glob(os.path.join(path, "*.jpg"))
    images = sorted(images)
    stamps = []

    rospy.loginfo("Found " + str(len(images)) + " images in path " + path)

    for oneImagePath in images:
        oneImageName = os.path.splitext(os.path.basename(oneImagePath))[0]
        stamp = datetime.strptime(oneImageName, imgStampFormat)
        stamps.append(stamp)

    cvbridge = CvBridge()
    for idx, oneImagePath in enumerate(images):
        rospy.logdebug("Load " + oneImagePath + " ...")

        img_stamp = datetime.timestamp(stamps[idx])

        cv_img = cv2.imread(oneImagePath)
        ros_img = cvbridge.cv2_to_imgmsg(cv_img, "bgr8")
        if use_sim_time:
            ros_img.header.stamp = rospy.Time.from_sec(img_stamp)
        else:
            ros_img.header.stamp = rospy.Time.now()

        imagesPub.publish(ros_img)
        if idx < len(images) - 1:
            img_stamp_next = datetime.timestamp(stamps[idx + 1])
            # TODO get sleeptime minus loadingtime from image
            sleepTime = img_stamp_next - img_stamp
            time.sleep(sleepTime)


if __name__ == "__main__":
    """main"""
    try:
        imageFolderPublisherNode()
    except rospy.ROSInterruptException:
        pass
