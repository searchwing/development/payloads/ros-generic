#!/usr/bin/env python

import cv2

cv2.useOptimized()

import message_filters
import rospy
from sensor_msgs.msg import NavSatFix
from vision_msgs.msg import Detection2DArray

from searchwing_common_py.Tracker import tracker
from searchwing_common_py.TrackerConverter import converter
from searchwing_msgs.msg import Detection3DArray, TrackArray


def callbackDetections(detections3D, detections2D):  # GPS
    GPS = NavSatFix()
    stamp_dateTime, measurements = _converter.ROS2StoneSoup(
        detections3D, detections2D, GPS
    )

    rospy.loginfo("Received  " + str(len(measurements)) + " measurements.")

    _tracker.process_measurements(stamp_dateTime, measurements)

    tracks = _tracker.get_tracks()
    rospy.loginfo("Tracks  " + str(len(tracks)))
    TrackArrayMsg = _converter.StoneSoup2ROS(detections3D.header.stamp, tracks)

    tracks_pub.publish(TrackArrayMsg)


# start node
rospy.init_node("TrackerNode", anonymous=True)


# subscriber
detections3DTopicName = rospy.get_param(
    "~i_detections3DTopicName", "/detections/Detections3D"
)
detections2DTopicName = rospy.get_param(
    "~i_detections2DTopicName", "/detections/Thermal"
)
# GPSTopicName  = rospy.get_param('~GPSTopicName',"/mavros/global_position/global")

detections3D_sub = message_filters.Subscriber(detections3DTopicName, Detection3DArray)
detections2D_sub = message_filters.Subscriber(detections2DTopicName, Detection2DArray)
# GPS_sub = message_filters.Subscriber(GPSTopicName,NavSatFix)

synced_detections = message_filters.TimeSynchronizer(
    [detections3D_sub, detections2D_sub], 10
)  # GPS_sub
synced_detections.registerCallback(callbackDetections)

# publisher
tracks_pub = rospy.Publisher("/tracks", TrackArray, queue_size=3)

maxAssocDist = rospy.get_param("~i_maxAssocDist", 20)
systemNoiseCoeff = rospy.get_param("~i_systemNoiseCoeff ", 0.05)
measPosNoiseVar = rospy.get_param("~i_measPosNoiseVar ", 15)
trackValidCount = rospy.get_param("~i_trackValidCount", 5)

# TODO reset tracker when system time discontinuity
_tracker = tracker(
    maxAssocDist=maxAssocDist,
    systemNoiseCoeff=systemNoiseCoeff,
    measPosNoiseVar=measPosNoiseVar,
    trackValidCount=trackValidCount,
)
_converter = converter()

print("TrackerNode started: Loop until new data arrives")
rospy.spin()
