#!/usr/bin/env python

import cv2
import rospkg

cv2.useOptimized()
import os

import numpy as np

# ros stuff
import rospy
import tf
from geometry_msgs.msg import Point, PointStamped
from sensor_msgs.msg import CameraInfo
from tf.msg import tfMessage
from visualization_msgs.msg import Marker, MarkerArray

from searchwing_common_py import camCalibProvider
from searchwing_msgs.msg import Detection3DArray, TrackArray

# start node
rospy.init_node("Detection3DRvizMarkersNode")

from searchwing_common_py import imagePointTransformations

tf_listener = tf.TransformListener()  # must be outside callback


def callbackDetections3D(Detections3DMsg):
    markerArray = MarkerArray()
    for one3DDet in Detections3DMsg.detections:
        marker = Marker()
        marker.ns = "Detections3D"
        marker.header.frame_id = "map"
        marker.type = marker.CUBE
        marker.action = marker.ADD
        marker.scale.x = 10.0
        marker.scale.y = 10.0
        marker.scale.z = 10.0
        marker.color.a = 0.8
        marker.color.r = 1.0
        marker.color.g = 0.0
        marker.color.b = 0.0
        marker.pose.orientation.w = 1.0
        marker.pose.position.x = one3DDet.position.x
        marker.pose.position.y = one3DDet.position.y
        marker.pose.position.z = one3DDet.position.z
        markerArray.markers.append(marker)

    for idx, oneMarker in enumerate(markerArray.markers):
        oneMarker.id = idx

    marker_publisher.publish(markerArray)

    drawImageArea("ImageDetections", Detections3DMsg.header.stamp, [1, 0, 0])


def callbackTracks(TracksMsg):
    # first delete old markers
    # needed explicit delete otherwise markers with IDs which are not overwritten, will remain
    markerArray = MarkerArray()
    for i in range(200):
        marker = Marker()
        marker.ns = "Tracks"
        marker.header.frame_id = "map"
        marker.type = marker.SPHERE
        marker.action = marker.DELETE
        markerArray.markers.append(marker)

    for idx, oneMarker in enumerate(markerArray.markers):
        oneMarker.id = idx
    marker_publisher.publish(markerArray)

    # create new track markers
    markerArray = MarkerArray()
    for track in TracksMsg.tracks:
        currPos = track.localPositions[0]

        marker = Marker()
        marker.ns = "Tracks"
        marker.header.frame_id = "map"
        marker.type = marker.SPHERE
        marker.scale.x = 15.0
        marker.scale.y = 15.0
        marker.scale.z = 15.0
        marker.color.a = 1.0
        if (
            track.state == track.STATETYPE_VALIDATED
            or track.state == track.STATETYPE_MEASURED_VALIDATED
        ):
            marker.color.r = 0.0
            marker.color.g = 1.0
            marker.color.b = 0.0
            marker.color.a = 0.9
        else:
            marker.color.r = 1.0
            marker.color.g = 0.0
            marker.color.b = 0.0
            marker.color.a = 0.5

        marker.pose.orientation.w = 1.0
        marker.pose.position.x = currPos.x
        marker.pose.position.y = currPos.y
        marker.pose.position.z = currPos.z
        markerArray.markers.append(marker)

        marker = Marker()
        marker.ns = "Tracks"
        marker.header.frame_id = "map"
        marker.type = marker.TEXT_VIEW_FACING
        marker.scale.z = 10.0

        if (
            track.state == track.STATETYPE_VALIDATED
            or track.state == track.STATETYPE_MEASURED_VALIDATED
        ):
            marker.color.r = 0.0
            marker.color.g = 1.0
            marker.color.b = 0.0
            marker.color.a = 0.9
        else:
            marker.color.r = 1.0
            marker.color.g = 0.0
            marker.color.b = 0.0
            marker.color.a = 0.7

        marker.pose.orientation.w = 1.0
        marker.pose.position.x = currPos.x + 10.0
        marker.pose.position.y = currPos.y
        marker.pose.position.z = currPos.z + 20.0
        marker.text = (
            str(track.id)
            + "-"
            + str(track.detectionCount)
            + "-"
            + f"{track.existanceProb:.2f}"
        )
        markerArray.markers.append(marker)

    for idx, oneMarker in enumerate(markerArray.markers):
        oneMarker.id = idx
    marker_publisher.publish(markerArray)


def drawImageArea(cameraTfName, cameraCalib, markerName, stamp, color):
    width = cameraCalib.width
    height = cameraCalib.height

    # Get 3D-Position of drone in the World
    drone3dPos = PointStamped()
    drone3dPos.point.x = 0
    drone3dPos.point.y = 0
    drone3dPos.point.z = 0
    drone3dPos.header.frame_id = "base_link"
    drone3dPos.header.stamp = stamp
    global tf_listener

    # image edges
    ptToText = {0: "UL", 1: "UR", 2: "LR", 3: "LL", 4: ""}
    pts2D = []
    pts2D.append(np.array([0, 0], int))
    pts2D.append(np.array([width, 0], int))
    pts2D.append(np.array([width, height], int))
    pts2D.append(np.array([0, height], int))
    pts2D.append(np.array([0, 0], int))

    markerArray = MarkerArray()

    marker = Marker()
    marker.ns = markerName
    marker.header.frame_id = "map"
    marker.type = marker.LINE_STRIP
    marker.scale.x = 1.0
    marker.color.a = 1.0
    marker.color.r = color[0]
    marker.color.g = color[1]
    marker.color.b = color[2]
    marker.pose.orientation.w = 1.0

    # convert 2d => 3d
    tf_listener.waitForTransform(
        cameraTfName, "map", stamp, rospy.Duration(1)
    )  # wait until the needed transformation is ready
    drone3dPosInMap = tf_listener.transformPoint("map", drone3dPos)

    for idx, pt2D in enumerate(pts2D):
        pt3D, valid = imagePointTransformations.getPixel3DPosOnWater(
            pt2D, drone3dPosInMap, cameraTfName, cameraCalib, stamp
        )  # project pixel to ground
        if not valid:
            rospy.logwarn(
                "Invalid 3D position calculated maybe due to pixel above horizon. Skip drawing of imageArea for camera "
                + cameraTfName
            )
        else:
            pt = Point()
            pt.x = pt3D[0]
            pt.y = pt3D[1]
            pt.z = pt3D[2]
            marker.points.append(pt)

            markerText = Marker()
            markerText.ns = markerName + "_named"
            markerText.header.frame_id = "map"
            markerText.type = marker.TEXT_VIEW_FACING
            markerText.scale.z = 10.0
            markerText.color.r = 1.0
            markerText.color.g = 1.0
            markerText.color.b = 1.0
            markerText.color.a = 1.0
            markerText.pose.orientation.w = 1.0
            markerText.pose.position.x = pt3D[0]
            markerText.pose.position.y = pt3D[1]
            markerText.pose.position.z = pt3D[2]
            markerText.text = str(ptToText[idx])
            markerArray.markers.append(markerText)

    markerArray.markers.append(marker)
    for idx, oneMarker in enumerate(markerArray.markers):
        oneMarker.id = idx
    marker_publisher.publish(markerArray)


def callbackTf(tfMsg):
    marker = Marker()
    marker.ns = "DroneTrajectory"
    marker.header.frame_id = "map"
    marker.type = marker.LINE_STRIP
    marker.scale.x = 1.0
    marker.color.a = 1.0
    marker.color.r = 0.0
    marker.color.g = 1.0
    marker.color.b = 1.0
    marker.pose.orientation.w = 1.0

    if (
        tfMsg.transforms[0].child_frame_id == "base_link"
        and tfMsg.transforms[0].header.frame_id == "map"
    ):
        pt = Point()
        pt.x = tfMsg.transforms[0].transform.translation.x
        pt.y = tfMsg.transforms[0].transform.translation.y
        pt.z = tfMsg.transforms[0].transform.translation.z
        droneTrajectoryPoints.append(pt)

        marker.points = droneTrajectoryPoints

        markerArray = MarkerArray()
        markerArray.markers.append(marker)
        for idx, oneMarker in enumerate(markerArray.markers):
            oneMarker.id = idx
        # global marker_publisher
        marker_publisher.publish(markerArray)

        for oneCameraTfName in cameraTfNames.split(","):
            if "camr" or "caml" in oneCameraTfName:
                drawImageArea(
                    oneCameraTfName,
                    rgb_camCalib,
                    oneCameraTfName + "_TfLive",
                    tfMsg.transforms[0].header.stamp,
                    [1, 1, 1],
                )
            if "camt" in oneCameraTfName:
                drawImageArea(
                    oneCameraTfName,
                    thermal_camCalib,
                    oneCameraTfName + "_TfLive",
                    tfMsg.transforms[0].header.stamp,
                    [1, 1, 1],
                )


def callbackCameraInfo(camInfoMsg, oneCameraTfName):
    if "camr" or "caml" in oneCameraTfName:
        drawImageArea(
            oneCameraTfName,
            rgb_camCalib,
            oneCameraTfName + "_ImageLive",
            camInfoMsg.header.stamp,
            [1, 0, 0],
        )
    if "camt" in oneCameraTfName:
        drawImageArea(
            oneCameraTfName,
            thermal_camCalib,
            oneCameraTfName + "_ImageLive",
            camInfoMsg.header.stamp,
            [1, 0, 0],
        )


# params
markerTopicName = rospy.get_param("~o_markerTopicName", "searchwing_rviz_markers")
r = rospkg.RosPack()
rgb_cameraCalibFilePath = os.path.join(
    r.get_path("searchwing_common_py"), "config", "frankiCamCalib.yaml"
)
thermal_cameraCalibFilePath = os.path.join(
    r.get_path("searchwing_thermal"), "config", "flirLepton3_0_franki.yaml"
)
rgb_camCalib = camCalibProvider.getCameraModel(rgb_cameraCalibFilePath)
thermal_camCalib = camCalibProvider.getCameraModel(thermal_cameraCalibFilePath)
cameraTfNames = rospy.get_param("~i_cameraTfNames", "caml,camr")

# subscriber
detections3DTopicName = rospy.get_param(
    "~i_detections3DTopicName", "/detections/Detections3D"
)
tracksTopicName = rospy.get_param("~i_tracksTopicName", "/tracks")

rospy.Subscriber(tracksTopicName, TrackArray, callbackTracks)
rospy.Subscriber(detections3DTopicName, Detection3DArray, callbackDetections3D)
rospy.Subscriber("/tf", tfMessage, callbackTf)
for oneCameraTfName in cameraTfNames.split(","):
    rospy.Subscriber(
        "/" + oneCameraTfName + "/camera_info",
        CameraInfo,
        callbackCameraInfo,
        oneCameraTfName,
    )

# publisher
marker_publisher = rospy.Publisher(markerTopicName, MarkerArray, queue_size=10)

droneTrajectoryPoints = []

rospy.spin()
