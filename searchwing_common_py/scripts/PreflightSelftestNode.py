#!/usr/bin/env python

import os
import time
from socket import gethostname

import rospkg
import rospy
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus
from mavros_msgs.msg import StatusText
from std_msgs.msg import Float64

from searchwing_common_py.preflightSelftest.modules.SelfTestModuleCameraImages import (
    SelfTestModuleCameraImages,
)
from searchwing_common_py.preflightSelftest.modules.SelfTestModuleClock import (
    SelfTestModuleClock,
)
from searchwing_common_py.preflightSelftest.modules.SelfTestModuleDiskSpace import (
    SelfTestModuleDiskSpace,
)
from searchwing_common_py.preflightSelftest.modules.SelfTestModuleParams import (
    SelfTestModuleParams,
)
from searchwing_common_py.preflightSelftest.modules.SelfTestModuleValueInMsg import (
    SelfTestModuleValueInMsg,
)
from searchwing_common_py.preflightSelftest.SelfTest import SelfTest
from searchwing_msgs.msg import DroneState

MAP_DIAGLEVEL_MAVLINK_SEVERITY = {
    DiagnosticStatus.STALE: StatusText.NOTICE,
    DiagnosticStatus.ERROR: StatusText.ALERT,
    DiagnosticStatus.WARN: StatusText.WARNING,
    DiagnosticStatus.OK: StatusText.INFO,
}


class PreflightSelftestNode:
    def __init__(self) -> None:
        # start node
        rospy.init_node("PreflightSelftestNode")
        rospy.loginfo("Started Preflighttestnode.")
        r = rospkg.RosPack()

        # Get Parameters
        self.interval = rospy.get_param("~i_interval", 30)
        self.cameraSelftestTimeoutOffset = rospy.get_param(
            "~i_cameraSelftestTimeoutOffset", 3
        )
        self.cameraSelftestPeriodTimeL = rospy.get_param(
            "/picamera_caml/i_imgPeriodTime"
        )
        self.cameraSelftestPeriodTimeR = rospy.get_param(
            "/picamera_camr/i_imgPeriodTime"
        )

        self.neededDiskSpace = (
            0.005
            * 1  # GB/frame  (image size)
            / self.cameraSelftestPeriodTimeL
            * 60  # frame/sec (image capture frequency)
            * 60
            * 2  # sec (flightduration)  # cameracount
        )
        self.camImgStorageDir = "/data/bilder/"
        self.blacklistFilePath = os.path.join(
            r.get_path("searchwing_common_py"),
            "config",
            "SelfTestModuleParamsBlacklist.txt",
        )

        rospy.Subscriber("/searchwing/state", DroneState, self.callbackSearchwingState)
        self.diagPub = rospy.Publisher("/diagnostics", DiagnosticArray, queue_size=5)
        self.diagStringsPub = rospy.Publisher(
            "/mavros/statustext/send", StatusText, queue_size=10
        )

        self.last = time.time()
        self.last = self.last - self.interval  # to directly do test
        rospy.spin()

    def callbackSearchwingState(self, stateMsg):
        rospy.logdebug("State received!")

        droneStateMsg = stateMsg

        if not droneStateMsg.isFlying.data:
            current = time.time()

            if not (current - self.last > self.interval):
                rospy.logdebug(
                    "Not enough time since last test... need to wait at least {} sec since last test!".format(
                        self.interval
                    )
                )
                return
            self.last = current

            missionFolderName = str(droneStateMsg.missionName.data)

            self.selftestModules = SelfTest(
                [
                    # SelfTestModuleCheckService('mavlink-router'),
                    SelfTestModuleDiskSpace("/data", self.neededDiskSpace),
                    SelfTestModuleCameraImages(
                        "Cam-R",
                        os.path.join(
                            self.camImgStorageDir,
                            missionFolderName,
                            "cam-r",
                            "preflight.jpg",
                        ),
                        self.cameraSelftestPeriodTimeR
                        + self.cameraSelftestTimeoutOffset,
                    ),
                    SelfTestModuleCameraImages(
                        "Cam-L",
                        os.path.join(
                            self.camImgStorageDir,
                            missionFolderName,
                            "cam-l",
                            "preflight.jpg",
                        ),
                        self.cameraSelftestPeriodTimeL
                        + self.cameraSelftestTimeoutOffset,
                    ),
                    SelfTestModuleClock(),
                    SelfTestModuleValueInMsg(
                        "CameraTemp",
                        "/cpu/temperature",
                        "<",
                        90.0,
                        120.0,
                        "°C",
                        Float64,
                    ),
                    SelfTestModuleValueInMsg(
                        "BoxTemp",
                        "/box/temperature",
                        "<",
                        70.0,
                        90.0,
                        "°C",
                        Float64,
                        timeout=6,
                    ),
                    SelfTestModuleValueInMsg(
                        "BoxHumidity",
                        "/box/humidity",
                        "<",
                        85.0,
                        95.0,
                        "%",
                        Float64,
                        timeout=6,
                    ),
                    SelfTestModuleParams(
                        "FactoryParams",
                        [
                            "/home/searchwing/factory/mav*.parm",
                            "/home/searchwing/factory/*.params",
                        ],
                        self.blacklistFilePath,
                    ),
                ]
            )
            rospy.loginfo("Start preflight selftests...")
            self.selftestModules.run_all()
            rospy.loginfo("Preflight selftests finished!")

            hostname = gethostname()

            # ros console feedback
            self.selftestModules.print_result()

            # send ros diagnostic messages
            diagnosticArrayMsg = DiagnosticArray()
            diagnosticArrayMsg.header = stateMsg.header
            for oneDiagResult in self.selftestModules.get_results_diagnostics():
                diagnosticArrayMsg.status.append(oneDiagResult)
            self.diagPub.publish(diagnosticArrayMsg)

            # qgc user feedback
            string = hostname + ": Preflight selftest of the payload..."
            textMsg = StatusText()
            textMsg.header = stateMsg.header
            textMsg.severity = StatusText.INFO
            textMsg.text = "####################"
            # self.diagStringsPub.publish(textMsg)
            textMsg.text = string
            self.diagStringsPub.publish(textMsg)
            errorCnt = 0
            for oneDiagResult in diagnosticArrayMsg.status:
                textMsg = StatusText()
                textMsg.header = stateMsg.header
                textMsg.severity = MAP_DIAGLEVEL_MAVLINK_SEVERITY[oneDiagResult.level]
                if oneDiagResult.level == DiagnosticStatus.ERROR:
                    errorCnt = errorCnt + 1
                if not "\n" in oneDiagResult.message:  # single line message
                    textMsg.text = oneDiagResult.name + ":" + oneDiagResult.message
                    self.diagStringsPub.publish(textMsg)
                else:  # reformat multi line message
                    textMsg.text = oneDiagResult.name + ":"
                    self.diagStringsPub.publish(textMsg)
                    messageLines = oneDiagResult.message.split("\n")
                    for oneLine in messageLines:
                        textMsg.text = oneLine
                        time.sleep(0.02)
                        self.diagStringsPub.publish(textMsg)

            textMsg = StatusText()
            textMsg.header = stateMsg.header
            if errorCnt > 0:
                textMsg.text = "Some payloads do not work. Do not take off!"
                textMsg.severity = StatusText.ALERT
            else:
                textMsg.text = "All payloads work as expected. Have a nice flight!"
                textMsg.severity = StatusText.INFO
            self.diagStringsPub.publish(textMsg)


startUpDelay = 20.0  # sec
rospy.loginfo("Sleep for {} seconds to let system warm up...".format(str(startUpDelay)))
time.sleep(startUpDelay)

selftest = PreflightSelftestNode()
