#!/usr/bin/env python
# searchwing - hoshauch - 01.2022

# ImageSaverNode
#
# PARAMS
# ~imgTopic => /camera/image/compressed
# ~metaTopic => /camera/imageMetadata
# ~outputRootFolder => /home/USR/searchwing
# ~droneStateTopic => /searchwing/state

import datetime
import io
import json
import os
import shutil
from fractions import Fraction
from pathlib import Path
from socket import gethostname

import message_filters
import numpy as np
import piexif
import piexif.helper
import psutil
import rospy
from mavros_msgs.msg import HomePosition
from sensor_msgs.msg import CompressedImage

from searchwing_msgs.msg import ImageMetadata


# from https://gist.github.com/NeoFarz/27f35ec2f84f5c52394cea3891c18832
def to_deg(value, loc):
    # convert decimal coordinates into degrees, minutes and seconds tuple
    # Keyword arguments:
    #   value is float gps-value,
    #   loc is direction list ["S", "N"] or ["W", "E"]
    # return: tuple like (25, 13, 48.343 ,'N')
    if value < 0:
        loc_value = loc[0]
    elif value > 0:
        loc_value = loc[1]
    else:
        loc_value = ""
    abs_value = abs(value)
    deg = int(abs_value)
    t1 = (abs_value - deg) * 60
    min = int(t1)
    sec = round((t1 - min) * 60, 5)
    return (deg, min, sec, loc_value)


def change_to_rational(number):
    # convert a number to rantional
    # Keyword arguments: number
    # return: tuple like (1, 2), (numerator, denominator)
    f = Fraction(str(number))
    return (f.numerator, f.denominator)


def convertGpsPosToString(gpsPos):
    return "{lat},{lon},{alt}".format(lat=gpsPos.lat, lon=gpsPos.lon, alt=gpsPos.alt)


def writeFile(image, metadataMsg):
    try:
        missionFolder = str(metadataMsg.droneState.missionName.data)
        fileName = str(metadataMsg.imageName.data)
        if not metadataMsg.droneState.isFlying.data:
            fileName = "preflight.jpg"

        filePath = os.path.join(
            str(outputRootFolder), str(missionFolder), outputFolderName, fileName
        )
        fileFolder = os.path.dirname(filePath)
        os.makedirs(fileFolder, exist_ok=True)
        rospy.loginfo("filename: " + filePath)
        with open(filePath, "wb") as new_image_file:
            new_image_file.write(image.read())
        # change owner of image and folders to searchwing user
        if fileOwnerUser != "":
            shutil.chown("/data", user=fileOwnerUser, group=fileOwnerUser)
            shutil.chown("/data/bilder", user=fileOwnerUser, group=fileOwnerUser)
            shutil.chown(filePath, user=fileOwnerUser, group=fileOwnerUser)
            shutil.chown(fileFolder, user=fileOwnerUser, group=fileOwnerUser)
            shutil.chown(
                os.path.join(fileFolder, ".."), user=fileOwnerUser, group=fileOwnerUser
            )

    except Exception as e:
        rospy.logerr("couldn't write image {}".format(filePath))
        rospy.logerr(e)

    # delete symlink to last latest if exist
    try:
        try:
            latestFilePath = os.path.join(
                str(outputRootFolder), outputFolderName + "_latest.jpg"
            )
            os.remove(latestFilePath)
        except:
            pass
        # create symbolic link to point latest image to current image
        os.symlink(filePath, latestFilePath)
    except Exception as e:
        rospy.logwarn(
            "couldn't write symlink to latest {} image".format(
                str(metadataMsg.sensorName.data)
            )
        )
        rospy.logwarn(e)


def addMetadata(imagedata, metadataMsg):
    global homePos

    lat_deg = to_deg(metadataMsg.droneGpsPos.latitude, ["S", "N"])
    lng_deg = to_deg(metadataMsg.droneGpsPos.longitude, ["W", "E"])

    exiv_lat = (
        change_to_rational(lat_deg[0]),
        change_to_rational(lat_deg[1]),
        change_to_rational(lat_deg[2]),
    )
    exiv_lng = (
        change_to_rational(lng_deg[0]),
        change_to_rational(lng_deg[1]),
        change_to_rational(lng_deg[2]),
    )

    gps_ifd = {
        piexif.GPSIFD.GPSVersionID: (2, 0, 0, 0),
        piexif.GPSIFD.GPSAltitudeRef: 0,
        piexif.GPSIFD.GPSAltitude: change_to_rational(
            round(max(0.0, metadataMsg.droneLocalPose.position.z))
        ),  # use local frame altitude instead of gps for resonable altitude at landtests
        piexif.GPSIFD.GPSLatitudeRef: lat_deg[3],
        piexif.GPSIFD.GPSLatitude: exiv_lat,
        piexif.GPSIFD.GPSLongitudeRef: lng_deg[3],
        piexif.GPSIFD.GPSLongitude: exiv_lng,
        piexif.GPSIFD.GPSImgDirectionRef: "T",  # T=True North, M=Magnetic North
        piexif.GPSIFD.GPSImgDirection: change_to_rational(
            float("%.2f" % (metadataMsg.heading))
        ),  # only save with 2 decimal precessions
    }

    additionalMetadata = {
        "imgcrns": {
            "ul": convertGpsPosToString(metadataMsg.imageCornerGpsPos[0]),
            "ur": convertGpsPosToString(metadataMsg.imageCornerGpsPos[1]),
            "lr": convertGpsPosToString(metadataMsg.imageCornerGpsPos[2]),
            "ll": convertGpsPosToString(metadataMsg.imageCornerGpsPos[3]),
        },
        "imgcrnsGSD": {
            "ul": str(metadataMsg.imageCornerGSD[0]),
            "ur": str(metadataMsg.imageCornerGSD[1]),
            "lr": str(metadataMsg.imageCornerGSD[2]),
            "ll": str(metadataMsg.imageCornerGSD[3]),
        },
    }

    if not homePos is None:
        additionalMetadata["homePos"] = {
            "latitude": str(homePos.latitude),
            "longitude": str(homePos.longitude),
        }

    userCommentString = json.dumps(additionalMetadata)
    userCommentDump = piexif.helper.UserComment.dump(
        userCommentString, encoding="ascii"
    )

    stamp = datetime.datetime.fromtimestamp(
        rospy.Time.to_time(metadataMsg.header.stamp)
    )
    artist = hostname + "-" + outputFolderName
    exif_ifd = {
        piexif.ExifIFD.DateTimeOriginal: stamp.strftime("%Y:%m:%d %H:%M:%S"), # redundant info, already set in https://github.com/raspberrypi/picamera2/blob/435c76fe7b2a44036ba6a428aa9bd5f01153d9e7/picamera2/request.py#L275
        piexif.ExifIFD.SubSecTimeOriginal: stamp.strftime("%f"),
        piexif.ExifIFD.UserComment: userCommentDump,
    }

    zero_ifd = {piexif.ImageIFD.Artist: artist}
    exif_dict = piexif.load(imagedata)
    exif_dict["0th"].update(zero_ifd)
    exif_dict["Exif"].update(exif_ifd)
    exif_dict["GPS"].update(gps_ifd)

    imagedataWithExif = io.BytesIO()
    exif_bytes = piexif.dump(exif_dict)
    try:
        piexif.insert(exif_bytes, imagedata, imagedataWithExif)
    except Exception as e:
        rospy.logerr("couldn't write exifdata to imagebuffer!")
        rospy.logerr(e)
        return None
    return imagedataWithExif


def validMetadata(metadataMsg):
    if (
        metadataMsg.imageCornerGpsPos[0].lat == 0.0
        and metadataMsg.imageCornerGpsPos[1].lat == 0.0
    ):
        return False
    if np.isnan(metadataMsg.imageCornerGpsPos[0].lat) or np.isnan(
        metadataMsg.imageCornerGpsPos[1].lat
    ):
        return False
    return True


def callbackImageAndMeta(imageMsg, metadataMsg):
    # dont save images when flying very low
    if validMetadata(metadataMsg=metadataMsg):
        if (
            metadataMsg.droneState.isFlying.data
            and metadataMsg.droneLocalPose.position.z < altitudeThresSaveImage
        ):
            rospy.logwarn(
                "Drone below altitude of {}m to save the images.".format(
                    altitudeThresSaveImage
                )
            )
            return

    # check disk space
    freeDiskSpaceMB = int(psutil.disk_usage(outputRootFolder).free / (1024 * 1024))
    if freeDiskSpaceMB < diskSpaceSafetyThresholdMB:
        rospy.logwarn(
            "Free disk space {} MB lower than Safety threshold {} MB - Skip image saving!".format(
                freeDiskSpaceMB, diskSpaceSafetyThresholdMB
            )
        )
        return

    # save image
    imageData = imageMsg.data
    if saveExif:
        imageDataWithExif = addMetadata(imageData, metadataMsg)
        if not imageDataWithExif is None:
            writeFile(imageDataWithExif, metadataMsg)
    else:
        writeFile(imageData, metadataMsg)


homePos = None


def callbackHomePos(homePositionMsg):
    global homePos
    homePos = homePositionMsg.geo


rospy.init_node("ImageSaverNode")

home = str(Path.home())
isFlying = False
missionName = ""
hostname = gethostname()

imgTopicName = rospy.get_param("~imgTopic", "/caml/image/compressed")
metaTopicName = rospy.get_param("~metaTopic", "/caml/imageMetadata")
homePosTopicName = rospy.get_param("~homePosTopic", "/mavros/home_position/home")
outputRootFolder = rospy.get_param("~outputRootFolder", "/tmp/searchwing")
saveExif = rospy.get_param("~saveExif", True)
diskSpaceSafetyThresholdMB = rospy.get_param("~diskSpaceSafetyThresholdMB", 100)
outputFolderName = rospy.get_param("~outputFolderName", "cam-l")
fileOwnerUser = rospy.get_param("~fileOwnerUser", "searchwing")
altitudeThresSaveImage = rospy.get_param("~altitudeThresSaveImage", 15)

# subscriber
imageSub = message_filters.Subscriber(imgTopicName, CompressedImage)
metaSub = message_filters.Subscriber(metaTopicName, ImageMetadata)
rospy.Subscriber(homePosTopicName, HomePosition, callbackHomePos)

synced_images = message_filters.TimeSynchronizer([imageSub, metaSub], 3)
synced_images.registerCallback(callbackImageAndMeta)

rospy.spin()
