#!/usr/bin/env python

import cv2

cv2.useOptimized()

import sys

import message_filters
import rospy
from sensor_msgs.msg import CompressedImage, Image
from vision_msgs.msg import Detection2DArray

# start node
rospy.init_node("imageDebugVisNodeDetections", anonymous=True)

from searchwing_common_py.imageDebugVis import (  # needs to be after init for tf_listener in debugVis
    imageDebugVis,
)

imgTopicName = rospy.get_param("~i_imgTopic", "/camera/image/compressed")
detectionsTopicName = rospy.get_param("~i_detectionsTopicName", "")

if detectionsTopicName == "":
    rospy.logerr("please define a topicname to visualize")
    sys.exit()

# subscriber
if detectionsTopicName != "":
    image_sub = message_filters.Subscriber(imgTopicName, CompressedImage)
    detections_sub = message_filters.Subscriber(detectionsTopicName, Detection2DArray)

    debugImageDetections_pub = rospy.Publisher(
        "/debugImageDetections", Image, queue_size=2
    )

    debugVisDetections = imageDebugVis(
        debugImageDetections_pub, camModel=None, camModelTf="cam"
    )

    synced_detections = message_filters.TimeSynchronizer([image_sub, detections_sub], 3)
    synced_detections.registerCallback(debugVisDetections.callbackDetections)

print("imageDebugVisNodeDetections started: Loop until new data arrives")
rospy.spin()
