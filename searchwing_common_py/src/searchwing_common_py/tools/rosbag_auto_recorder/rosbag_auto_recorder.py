#!/usr/bin/env python
"""

Copyright(c) <Florian Lier>

This file may be licensed under the terms of the
GNU Lesser General Public License Version 3 (the ``LGPL''),
or (at your option) any later version.

Software distributed under the License is distributed
on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
express or implied. See the LGPL for the specific language
governing rights and limitations.

You should have received a copy of the LGPL along with this
program. If not, go to http://www.gnu.org/licenses/lgpl.html
or write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

The development of this software was supported by the
Excellence Cluster EXC 277 Cognitive Interaction Technology.
The Excellence Cluster EXC 277 is a grant of the Deutsche
Forschungsgemeinschaft (DFG) in the context of the German
Excellence Initiative.

Authors: Florian Lier
<flier>@techfak.uni-bielefeld.de

"""

import os
import signal
import subprocess

# STD IMPORTS
import sys
import threading
import time
from optparse import OptionParser

import psutil
import rospy

from searchwing_msgs.msg import DroneState


class ROSRecordConnector(threading.Thread):
    def __init__(self, _inscope, _triggerscope, _msglimit, _directory=None):
        threading.Thread.__init__(self)
        self.is_running = True
        if _directory is None:
            self.directory = ""
        else:
            self.directory = _directory
        self.listen_topic = _triggerscope
        self.inscope = _inscope
        self.msglimit = _msglimit
        self.recordprocess = None

    def record_bool_callback(self, ros_data):
        missionFolder = str(ros_data.missionName.data)
        filePath = os.path.join(
            self.directory, str(missionFolder), ros_data.missionName.data[0:19]
        )
        self.record_handling(ros_data.isFlying.data, filePath)

    def record_handling(self, record, filepath=None):
        if (
            record
            and self.recordprocess is not None
            and self.recordprocess.is_recording
        ):
            return False  # already recording

        if not record and self.recordprocess is None:
            return False  # already stopped
        if self.recordprocess is not None:
            print(">>> [ROS] Record status: %s" % str(self.recordprocess.is_recording))
        else:
            print(">>> [ROS] Record status: %s" % False)

        if record and filepath is not None:
            os.makedirs(os.path.dirname(filepath), exist_ok=True)
            self.recordprocess = RecordBAG(filepath, self.inscope, self.msglimit)
            self.recordprocess.start()
            return True
        else:
            if self.recordprocess is not None:
                if self.recordprocess.stop():
                    self.recordprocess = None
                    return True
                else:
                    return False
            return True

    def run(self):
        print(
            ">>> [ROS] Initializing ROSBAG REMOTE RECORD of: %s" % self.inscope.strip()
        )
        ros_subscriber = rospy.Subscriber(
            self.listen_topic, DroneState, self.record_bool_callback, queue_size=1
        )
        print(
            ">>> [ROS] Waiting for Bool (true for start) on topic : %s"
            % self.listen_topic
        )
        while self.is_running is True:
            time.sleep(0.05)
        if self.recordprocess is not None:
            self.recordprocess.stop()
        ros_subscriber.unregister()
        print(">>> [ROS] Stopping ROSBAG REMOTE RECORD %s" % self.inscope.strip())


class RecordBAG(threading.Thread):
    def __init__(self, _name, _scope, _msg_limit=None):
        threading.Thread.__init__(self)
        self.name = _name.strip()
        self.scope = _scope.strip()
        self.is_recording = False
        self.process = None
        self.msg_limit = _msg_limit
        self.chunksize = 4096*4
        self.buffersize = 1024*4

    def stop(self):
        print(">>> Received STOP")
        if not self.is_recording:
            print(">>> Already stopped")
            return True
        self.is_recording = False
        try:
            p = psutil.Process(self.process.pid)
            try:
                for sub in p.get_children(recursive=True):
                    sub.send_signal(signal.SIGINT)
                    self.process.send_signal(signal.SIGINT)
                return True
            except AttributeError:
                # newer psutils
                for sub in p.children(recursive=True):
                    sub.send_signal(signal.SIGINT)
                    self.process.send_signal(signal.SIGINT)
                return True
        except Exception as e:
            print(">>> Maybe the process is already dead? %s" % str(e))
            return False

    def run(self):
        print(">>> Recording: %s now" % self.scope)
        rosbag_filename = self.name + ".bag"
        print(">>> Filename:  %s" % rosbag_filename)
        if self.msg_limit is not None:
            print(">>> stopping after %s messages" % str(self.msg_limit))
            self.process = subprocess.Popen(
                "rosbag record -l %s -O %s %s"
                % (str(self.msg_limit), rosbag_filename, self.scope),
                shell=True,
            )
        else:
            self.process = subprocess.Popen(
                "rosbag record -O %s %s --chunksize=%s --buffsize=%s"
                % (rosbag_filename, self.scope, str(self.chunksize), str(self.buffersize)),
                shell=True,
            )
        self.is_recording = True
        self.process.wait()
        print(">>> Recording: %s stopped" % self.scope)
        self.is_recording = False


def signal_handler(sig, frame):
    """
    Callback function for the signal handler, catches signals
    and gracefully stops the application, i.e., exit subscribers
    before killing the main thread.
    :param sig the signal number
    :param frame frame objects represent execution frames.
    """
    print(">>> Exiting (signal %s)..." % str(sig))
    r.is_running = False
    print(">>> Bye!")
    sys.exit(0)


if __name__ == "__main__":

    parser = OptionParser(usage="Usage: %prog [options]")
    parser.add_option(
        "-d",
        "--directory",
        action="store",
        dest="directory",
        help="(only for named trigger) the base directory in which the files should be saved",
    )
    parser.add_option(
        "-l",
        "--limit",
        action="store",
        dest="msglimit",
        help="The number of message to record before automatically stopping",
    )

    (options, args) = parser.parse_args()

    inscope_topics_path = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "inscope_topics.txt"
    )
    print(inscope_topics_path)

    with open(inscope_topics_path, "r") as inscope_topics_file:
        inscope_topics = inscope_topics_file.read().replace("\n", " ")

    options.triggerscope = "/searchwing/state"

    rospy.init_node("rosbag_remote_record", anonymous=True)
    print("Started rosbag_remote_record node")
    r = ROSRecordConnector(
        inscope_topics, options.triggerscope, options.msglimit, options.directory
    )
    r.start()

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    while True:
        time.sleep(0.2)
