#!/usr/bin/env python
"""

Copyright(c) <Florian Lier>

This file may be licensed under the terms of the
GNU Lesser General Public License Version 3 (the ``LGPL''),
or (at your option) any later version.

Software distributed under the License is distributed
on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
express or implied. See the LGPL for the specific language
governing rights and limitations.

You should have received a copy of the LGPL along with this
program. If not, go to http://www.gnu.org/licenses/lgpl.html
or write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

The development of this software was supported by the
Excellence Cluster EXC 277 Cognitive Interaction Technology.
The Excellence Cluster EXC 277 is a grant of the Deutsche
Forschungsgemeinschaft (DFG) in the context of the German
Excellence Initiative.

Authors: Florian Lier
<flier>@techfak.uni-bielefeld.de

"""

import os
import signal
import subprocess

# STD IMPORTS
import sys
import threading
import time
from optparse import OptionParser

import psutil
import rospy

from searchwing_msgs.msg import DroneState


class ROSRecordConnector(threading.Thread):
    def __init__(self, _triggerscope, _directory=None):
        threading.Thread.__init__(self)
        self.is_running = True
        if _directory is None:
            self.directory = ""
        else:
            self.directory = _directory
        self.listen_topic = _triggerscope
        self.recordprocess = None

    def record_bool_callback(self, ros_data):
        newMissionName = str(ros_data.missionName.data)

        # start recording
        if self.recordprocess is None and not "noMission" in newMissionName:
            self.recordprocess = Recordlog(self.directory, newMissionName)
            self.recordprocess.start()
        # stop recording
        elif (
            not self.recordprocess is None
            and not "noMission" in self.recordprocess.missionName
            and "noMission" in newMissionName
        ):
            self.recordprocess.stop()
            self.recordprocess = None

    def run(self):
        ros_subscriber = rospy.Subscriber(
            self.listen_topic, DroneState, self.record_bool_callback, queue_size=1
        )
        print(
            ">>> [log] Waiting for Bool (true for start) on topic : %s"
            % self.listen_topic
        )
        while self.is_running is True:
            time.sleep(0.05)
        if self.recordprocess is not None:
            self.recordprocess.stop()
        ros_subscriber.unregister()


class Recordlog(threading.Thread):
    def __init__(self, path, missionName):
        threading.Thread.__init__(self)
        self.path = path.strip()
        self.is_recording = False
        self.process = None
        self.missionName = missionName

    def stop(self):
        print(">>> Received STOP")
        if not self.is_recording:
            print(">>> Already stopped")
            return True
        self.is_recording = False
        try:
            p = psutil.Process(self.process.pid)
            try:
                for sub in p.get_children(recursive=True):
                    sub.send_signal(signal.SIGINT)
                    self.process.send_signal(signal.SIGINT)
                return True
            except AttributeError:
                # newer psutils
                for sub in p.children(recursive=True):
                    sub.send_signal(signal.SIGINT)
                    self.process.send_signal(signal.SIGINT)
                return True
        except Exception as e:
            print(">>> Maybe the process is already dead? %s" % str(e))
            return False

    def run(self):
        print(">>> [log]: Started recording in folder  %s" % self.path)
        missionFolder = os.path.join(self.path, self.missionName)
        os.makedirs(missionFolder, exist_ok=True)
        cmd = "mavlink-routerd -c /opt/catkin_ws/src/ros-generic/searchwing_bringup/config/mavlink-router-record-binlog.config -l {}".format(
            missionFolder
        )
        print("Execute {}".format(cmd))
        self.process = subprocess.Popen(cmd, cwd=self.path, shell=True)
        self.is_recording = True
        self.process.wait()
        print(">>> [log]: Recording stopped.")
        self.is_recording = False


def signal_handler(sig, frame):
    """
    Callback function for the signal handler, catches signals
    and gracefully stops the application, i.e., exit subscribers
    before killing the main thread.
    :param sig the signal number
    :param frame frame objects represent execution frames.
    """
    print(">>> Exiting (signal %s)..." % str(sig))
    r.is_running = False
    print(">>> Bye!")
    sys.exit(0)


if __name__ == "__main__":
    parser = OptionParser(usage="Usage: %prog [options]")
    parser.add_option(
        "-d",
        "--directory",
        action="store",
        dest="directory",
        default="/data/bilder",
        help="(only for named trigger) the base directory in which the files should be saved",
    )

    (options, args) = parser.parse_args()

    options.triggerscope = "/searchwing/state"

    print("Started log_remote_record node")
    rospy.init_node("log_remote_record", anonymous=True)
    r = ROSRecordConnector(options.triggerscope, options.directory)
    r.start()

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    while True:
        time.sleep(0.2)
