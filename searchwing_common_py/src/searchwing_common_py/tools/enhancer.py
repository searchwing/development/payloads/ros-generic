import glob
import os
from datetime import datetime, timedelta

import cv2
import rosbag
import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import CompressedImage
from tqdm import tqdm

cvbridge = CvBridge()


def opencv2ros(img):
    ros_img = cvbridge.cv2_to_imgmsg(img, "mono16")
    return ros_img


inputBagPath = "/home/julle/ControlerProjekte/SearchWing/Data/rosbags/20200725_fahrlandersee/3_zechliner_see/2020-07-25-19-17-14.bag"
inbag = rosbag.Bag(inputBagPath)
inputNormalImgPath = "/home/julle/ControlerProjekte/SearchWing/Data/rosbags/20200725_fahrlandersee/3_zechliner_see/data/bilder/normal"
inputThermalImgPath = "/home/julle/ControlerProjekte/SearchWing/Data/rosbags/20200725_fahrlandersee/3_zechliner_see/data/bilder/thermal"

outputBagPath = "/home/julle/ControlerProjekte/SearchWing/Data/rosbags/20200725_fahrlandersee/3_zechliner_see/out.bag"

first = True
inputNormalImgs = glob.glob(os.path.join(inputNormalImgPath, "*.jpg"))
normalImgData = []
for oneInputNormalImg in inputNormalImgs:
    if "latest" in oneInputNormalImg:
        continue
    imgName = os.path.basename(oneInputNormalImg)
    imgStamp = datetime.strptime(imgName, "%Y-%m-%dT%H-%M-%S.%f.jpg")
    imgStamp = imgStamp + timedelta(hours=1)

    if first:
        print("normal stamp:", imgStamp)
        first = False
    rosStamp = rospy.Time.from_sec(datetime.timestamp(imgStamp))
    normalImgData.append([oneInputNormalImg, rosStamp])

first = True
inputThermalImgs = glob.glob(os.path.join(inputThermalImgPath, "*.png"))
thermalImgData = []
for oneInputThermalImg in inputThermalImgs:
    if "latest" in oneInputThermalImg:
        continue
    imgName = os.path.basename(oneInputThermalImg)
    imgStamp = datetime.strptime(imgName, "%Y-%m-%dT%H-%M-%S.%f.png")
    imgStamp = imgStamp + timedelta(hours=2)

    if first:
        print("thermal stamp:", imgStamp)
        first = False
    rosStamp = rospy.Time.from_sec(datetime.timestamp(imgStamp))
    thermalImgData.append([oneInputThermalImg, rosStamp])


first = True
tf_msgs = []
for topic, msg, t in inbag.read_messages(topics=["/tf"]):
    tf_msgs.append(msg)
    if first:
        stamp_dateTime = datetime.fromtimestamp(msg.transforms[0].header.stamp.to_sec())
        print("tf stamp:", stamp_dateTime)
        first = False
inbag.close()


with rosbag.Bag(outputBagPath, "w") as outbag:

    print("Write /tf")
    for oneTf_msg in tqdm(tf_msgs):
        tf_stamp = oneTf_msg.transforms[0].header.stamp
        # if  tf_stamp< normalImgData[0][1] or tf_stamp> normalImgData[-1][1]:
        #    continue
        stamp_dateTime = tf_stamp.to_sec()
        # print("tf stamp:",stamp_dateTime)
        outbag.write("/tf", oneTf_msg, oneTf_msg.transforms[0].header.stamp)

    print("Write /thermal/image")
    for oneThermalImgData in tqdm(thermalImgData):
        path = oneThermalImgData[0]
        stamp = oneThermalImgData[1]
        binaryPic = cv2.imread(path, -1)  # -1 == leave format as it is.
        if binaryPic is None:
            continue

        picMsg = opencv2ros(binaryPic)
        picMsg.header.stamp = stamp
        outbag.write("/thermal/image", picMsg, picMsg.header.stamp)

    print("Write /camera/image/compressed")
    for oneNormalImgData in tqdm(normalImgData):
        path = oneNormalImgData[0]
        stamp = oneNormalImgData[1]
        binaryPicFile = open(path, "rb")
        binaryPic = binaryPicFile.read()

        picMsg = CompressedImage()
        picMsg.header.stamp = stamp
        picMsg.format = "jpg"
        picMsg.data = binaryPic

        outbag.write("/camera/image/compressed", picMsg, picMsg.header.stamp)

outbag.close()
