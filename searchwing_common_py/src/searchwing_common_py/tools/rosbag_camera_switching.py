import os

import rosbag

basePath = "/media/"
inputBagPath = os.path.join(basePath, "2022-04-16T02-39-38.bag")
inbag = rosbag.Bag(inputBagPath)

outputBagPath = os.path.join(basePath, "repaired.bag")

with rosbag.Bag(outputBagPath, "w") as Y:
    for topic, msg, t in inbag:

        proc = int(
            (t.to_sec() - inbag.get_start_time())
            / (inbag.get_end_time() - inbag.get_start_time())
            * 100
        )
        print("done " + str(proc) + "%")

        if topic == "/caml/camera_info":
            Y.write("/camr/camera_info", msg, t)
        elif topic == "/camr/camera_info":
            Y.write("/caml/camera_info", msg, t)
        elif topic == "/camr/imageMetadata":
            continue
        elif topic == "/caml/imageMetadata":
            continue
        elif topic == "/tf":
            if msg.transforms[0].child_frame_id == "caml_base":
                continue
            if msg.transforms[0].child_frame_id == "camr_base":
                continue
            if msg.transforms[0].child_frame_id == "caml":
                continue
            if msg.transforms[0].child_frame_id == "camr":
                continue
            Y.write(topic, msg, t)
        else:
            Y.write(topic, msg, t)
