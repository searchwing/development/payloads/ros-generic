#!/usr/bin/env python
import cv2

cv2.useOptimized()
import numpy as np
import rospy
import tf
from geometry_msgs.msg import PointStamped

tf_listener = tf.TransformListener()  # must be outside callback


def getPixelGPSPosOnWater(
    Point2D, Drone3DPosInMap, DroneGpsPos, camTf, camModel, stamp
):
    Point3DLocalENU, valid = getPixel3DPosOnWater(
        Point2D, Drone3DPosInMap, camTf, camModel, stamp
    )

    if valid:
        # approximation from https://stackoverflow.com/a/7478827
        Drone3DPosENU = np.array(
            [Drone3DPosInMap.point.x, Drone3DPosInMap.point.y, Drone3DPosInMap.point.z]
        )
        delta_localENU = Point3DLocalENU - Drone3DPosENU
        delta_north = delta_localENU[1]  # [m]
        delta_east = delta_localENU[0]  # [m]
        r_earth = 6371000  # [m]
        pixelLatitude = DroneGpsPos.latitude + (delta_north / r_earth) * (180.0 / np.pi)
        pixelLongitude = DroneGpsPos.longitude + (delta_east / r_earth) * (
            180.0 / np.pi
        ) / np.cos(DroneGpsPos.latitude * np.pi / 180.0)
        return np.array([pixelLatitude, pixelLongitude, 0.0]), valid
    else:
        return np.array([np.nan, np.nan, 0.0]), valid


# Function to get the 3D Position of a Pixel on the Water in a Picture by transforming the 2D Pos of the Pixel to a 3D Pos in the World
def getPixel3DPosOnWater(Point2D, Drone3DPosInMap, camTf, camModel, stamp):
    # get Vector from origin to 3d pos of pixel in the camcoordinatesystem
    # see https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html for visualization
    Point3D = camModel.projectPixelTo3dRay(Point2D)

    # generate point in cam coordinate system in ros
    pt = PointStamped()
    pt_transformed = PointStamped()
    pt.point.x = Point3D[0]
    pt.point.y = Point3D[1]
    pt.point.z = Point3D[2]
    pt.header.frame_id = camTf
    pt.header.stamp = stamp

    # transform point to drone coordinate system
    pt_transformed = tf_listener.transformPoint("map", pt)

    # Define plane on ground = sea
    planeNormal = np.array([0, 0, 1])
    planePoint = np.array([0, 0, 0])

    # Define ray through pixel in drone coordinate system
    rayPoint = np.array(
        [Drone3DPosInMap.point.x, Drone3DPosInMap.point.y, Drone3DPosInMap.point.z]
    )  # Any point along the ray
    rayDirection = np.array(
        [
            pt_transformed.point.x - Drone3DPosInMap.point.x,
            pt_transformed.point.y - Drone3DPosInMap.point.y,
            pt_transformed.point.z - Drone3DPosInMap.point.z,
        ]
    )

    angle_plane_to_ray = np.rad2deg(
        np.arccos(np.clip(np.dot(planeNormal, rayDirection), -1.0, 1.0))
    )
    rayBelowHorizon = (
        np.abs(angle_plane_to_ray) <= 180.0 and np.abs(angle_plane_to_ray) > 90.0
    )
    rayPointOnPlane, linePlaneCollisionValid = LinePlaneCollision(
        planeNormal, planePoint, rayDirection, rayPoint
    )
    valid = rayBelowHorizon and linePlaneCollisionValid

    return rayPointOnPlane, valid


# Function to get the pixelpos of a 3D Pos in the World
def project3DPosToPic(Point3D, camModel, camTfName, stamp):
    pt = PointStamped()
    pt.point.x = Point3D[0]
    pt.point.y = Point3D[1]
    pt.point.z = Point3D[2]
    pt.header.frame_id = "map"
    pt.header.stamp = stamp
    # transform point to cam coordinate system
    tf_listener.waitForTransform(
        camTfName, "map", stamp, rospy.Duration(3)
    )  # wait until the needed transformation is ready, as the image can be provided faster than the telemetry

    Point3DinCamCoord = tf_listener.transformPoint(camTfName, pt)
    Point2DinPicCoord = camModel.project3dToPixel(
        [
            Point3DinCamCoord.point.x,
            Point3DinCamCoord.point.y,
            Point3DinCamCoord.point.z,
        ]
    )
    return Point2DinPicCoord


def getPixelGSD(Point2D, Drone3DPosInMap, DroneGpsPos, camTf, camModel, stamp):
    pt1, valid0 = getPixel3DPosOnWater(Point2D, Drone3DPosInMap, camTf, camModel, stamp)
    pt2, valid1 = getPixel3DPosOnWater(
        [Point2D[0], Point2D[1] + 1], Drone3DPosInMap, camTf, camModel, stamp
    )
    valid = valid0 and valid1
    if valid:
        return np.linalg.norm(pt1 - pt2), True
    else:
        return np.nan, False


# A generic function to compute the intersection of a 3D Line with a 3D Plane
def LinePlaneCollision(planeNormal, planePoint, rayDirection, rayPoint, epsilon=1e-6):
    ndotu = planeNormal.dot(rayDirection)
    if abs(ndotu) < epsilon:
        # raise RuntimeError("no intersection or line is within plane")
        return planePoint, False
    w = rayPoint - planePoint
    si = -planeNormal.dot(w) / ndotu
    Psi = w + si * rayDirection + planePoint
    return Psi, True
