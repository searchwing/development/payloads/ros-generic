import datetime

import stonesoup
from geometry_msgs.msg import Point

from searchwing_msgs.msg import Track, TrackArray


class converter:
    def __init__(self):
        self.trackId = 0
        self.trackIds = {}

    def ROS2StoneSoup(self, Detection3DArrayMsg, Detection2DArrayMsg, GPS):
        measurements = set()
        stamp_dateTime = datetime.datetime.fromtimestamp(
            Detection3DArrayMsg.header.stamp.to_sec()
        )
        for oneMeas in Detection3DArrayMsg.detections:
            Detection2D = Detection2DArrayMsg.detections[oneMeas.DetectionArrayIdx]
            measurements.add(
                stonesoup.types.detection.Detection(
                    state_vector=[[oneMeas.position.x], [oneMeas.position.y]],
                    timestamp=stamp_dateTime,
                    metadata={"GPS": GPS, "Detection2D": Detection2D},
                )
            )
        return stamp_dateTime, measurements

    def StoneSoup2ROS(self, stamp, tracks):
        tracksMsg = TrackArray()
        tracksMsg.header.stamp = stamp

        for oneStoneSoupTrack in tracks:
            oneTrackMsg = Track()

            if oneStoneSoupTrack.id not in self.trackIds:
                self.trackIds[oneStoneSoupTrack.id] = self.trackId
                self.trackId = self.trackId + 1

            oneTrackMsg.id = self.trackIds[oneStoneSoupTrack.id]

            # print(oneStoneSoupTrack.state.covariance)
            pos = Point()
            pos.x = oneStoneSoupTrack.state.mean[0]
            pos.y = oneStoneSoupTrack.state.mean[1]
            pos.z = 0
            positions = []
            positions.append(pos)
            oneTrackMsg.localPositions = positions

            oneTrackMsg.Detection2D = oneStoneSoupTrack.metadata["Detection2D"]

            oneTrackMsg.GPSPosition = oneStoneSoupTrack.metadata["GPS"]

            oneTrackMsg.detectionCount = oneStoneSoupTrack.metadata["detectionCount"]

            oneTrackMsg.existanceProb = oneStoneSoupTrack.metadata["existProb"]

            oneTrackMsg.state = oneStoneSoupTrack.metadata["state"]

            tracksMsg.tracks.append(oneTrackMsg)

        return tracksMsg
