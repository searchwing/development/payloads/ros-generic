import numpy as np
import scipy as sp
from stonesoup.base import Property
from stonesoup.dataassociator.neighbour import GNNWith2DAssignment
from stonesoup.deleter.time import UpdateTimeStepsDeleter
from stonesoup.hypothesiser.distance import DistanceHypothesiser
from stonesoup.initiator.simple import SimpleMeasurementInitiator
from stonesoup.measures import Euclidean
from stonesoup.models.base import TimeVariantModel
from stonesoup.models.measurement.linear import LinearGaussian
from stonesoup.models.transition.linear import (
    CombinedLinearGaussianTransitionModel,
    LinearGaussianTransitionModel,
)
from stonesoup.predictor.kalman import KalmanPredictor
from stonesoup.types.state import CovarianceMatrix, GaussianState
from stonesoup.updater.kalman import KalmanUpdater

from searchwing_msgs.msg import Track as searchwingTrack


class ConstantPosition(LinearGaussianTransitionModel, TimeVariantModel):
    r"""This is a class implementation of a time-variant 1D Linear-Gaussian
    Constant Velocity Transition Model.

    The target is assumed to move with (nearly) constant velocity, where
    target acceleration is model as white noise.

    The model is described by the following SDEs:

        .. math::
            :nowrap:

            \begin{eqnarray}
                dx_{pos} & = & x_{vel} d & | {Position \ on \
                X-axis (m)} \\
                dx_{vel} & = & q\cdot dW_t,\ W_t \sim \mathcal{N}(0,q^2) & | \
                Speed on\ X-axis (m/s)
            \end{eqnarray}

    Or equivalently:

        .. math::
            x_t = F_t x_{t-1} + w_t,\ w_t \sim \mathcal{N}(0,Q_t)

    where:

        .. math::
            x & = & \begin{bmatrix}
                        x_{pos} \\
                        x_{vel}
                \end{bmatrix}

        .. math::
            F_t & = & \begin{bmatrix}
                        1 & dt\\
                        0 & 1
                \end{bmatrix}

        .. math::
            Q_t & = & \begin{bmatrix}
                        \frac{dt^3}{3} & \frac{dt^2}{2} \\
                        \frac{dt^2}{2} & dt
                \end{bmatrix} q
    """

    noise_diff_coeff = Property(
        float, doc="The velocity noise diffusion coefficient :math:`q`"
    )

    @property
    def ndim_state(self):
        """ndim_state getter method

        Returns
        -------
        :class:`int`
            :math:`2` -> The number of model state dimensions
        """

        return 1

    def matrix(self, time_interval, **kwargs):
        """Model matrix :math:`F(t)`

        Parameters
        ----------
        time_interval: :class:`datetime.timedelta`
            A time interval :math:`dt`

        Returns
        -------
        :class:`numpy.ndarray` of shape\
        (:py:attr:`~ndim_state`, :py:attr:`~ndim_state`)
            The model matrix evaluated given the provided time interval.
        """

        return sp.array([[1]])

    def covar(self, time_interval, **kwargs):
        """Returns the transition model noise covariance matrix.

        Parameters
        ----------
        time_interval : :class:`datetime.timedelta`
            A time interval :math:`dt`
        Returns
        -------
        :class:`stonesoup.types.state.CovarianceMatrix` of shape\
        (:py:attr:`~ndim_state`, :py:attr:`~ndim_state`)
            The process noise covariance.
        """

        time_interval_sec = time_interval.total_seconds()

        covar = sp.array([[1]]) * self.noise_diff_coeff

        return CovarianceMatrix(covar)


class tracker:
    def __init__(
        self,
        maxAssocDist=10,
        systemNoiseCoeff=1,
        measPosNoiseVar=10,
        initExistProb=0.6,
        trackValidCount=5,
    ):
        self.transition_model = CombinedLinearGaussianTransitionModel(
            (ConstantPosition(systemNoiseCoeff), ConstantPosition(systemNoiseCoeff))
        )

        self.predictor = KalmanPredictor(self.transition_model)
        self.measurement_model = LinearGaussian(
            ndim_state=2,  # Number of state dimensions (position in 2D)
            mapping=(0, 1),  # Mapping measurement vector index to state index
            noise_covar=np.array([[measPosNoiseVar, 0], [0, measPosNoiseVar]]),
        )

        self.updater = KalmanUpdater(self.measurement_model)
        self.hypothesiser = DistanceHypothesiser(
            self.predictor,
            self.updater,
            measure=Euclidean(),
            missed_distance=maxAssocDist,
        )
        self.data_associator = GNNWith2DAssignment(self.hypothesiser)
        self.deleter = UpdateTimeStepsDeleter(
            time_steps_since_update=5
        )  # UpdateTimeDeleter(4)
        self.initiator = SimpleMeasurementInitiator(
            prior_state=GaussianState(
                [[0], [0]], np.diag([1, 1])
            ),  # will be replaced in usage
            measurement_model=self.measurement_model,
        )
        self.tracks = set()

        self.initExistProb = initExistProb
        self.trackValidCount = trackValidCount

    def process_measurements(self, stamp, measurements):
        # Calculate all hypothesis pairs and associate the elements in the best subset to the tracks.
        hypotheses = self.data_associator.associate(self.tracks, measurements, stamp)

        associated_measurements = set()
        for track in self.tracks:
            hypothesis = hypotheses[track]
            if hypothesis.measurement:  # Measurement associated to existing track
                post = self.updater.update(hypothesis)
                track.append(post)
                associated_measurements.add(hypothesis.measurement)
                track.metadata["detectionCount"] += 1
                track.metadata["existProb"] += 0.1
                track.metadata["state"] = searchwingTrack.STATETYPE_MEASURED

            else:  # When data associator says no detections are good enough, we'll keep the prediction
                track.append(hypothesis.prediction)
                track.metadata["existProb"] -= 0.03
                track.metadata["state"] = searchwingTrack.STATETYPE_PREDICTED

            # Limit existance prob to [0,1]
            track.metadata["existProb"] = max(0, min(1.0, track.metadata["existProb"]))

        # Carry out deletion and initiation
        self.tracks -= self.deleter.delete_tracks(self.tracks)
        self.tracks |= self.initiator.initiate(measurements - associated_measurements)

        # init new tracks
        for track in self.tracks:
            if not "existProb" in track.metadata:
                track.metadata["existProb"] = self.initExistProb
            if not "detectionCount" in track.metadata:
                track.metadata["detectionCount"] = 1
                track.metadata["state"] = searchwingTrack.STATETYPE_MEASURED

            if track.metadata["detectionCount"] >= self.trackValidCount:
                if track.metadata["state"] == searchwingTrack.STATETYPE_MEASURED:
                    track.metadata[
                        "state"
                    ] = searchwingTrack.STATETYPE_MEASURED_VALIDATED
                else:
                    track.metadata["state"] = searchwingTrack.STATETYPE_VALIDATED

    def get_tracks(self):
        return self.tracks
