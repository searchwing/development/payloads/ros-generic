#!/usr/bin/env python
####################################################################################
# Image Debug visualizations
####################################################################################
import cv2

cv2.useOptimized()
import numpy as np
import rospy
import tf
from cv_bridge import CvBridge
from nav_msgs.msg import Path

from searchwing_msgs.msg import Track

from . import imagePointTransformations
from .helper import BoundingBox2D_to_OpenCVBox

tf_listener = tf.TransformListener()
tf_broadcaster = tf.TransformBroadcaster()
cvbridge = CvBridge()


class imageDebugVis:
    def __init__(self, imgPub, camModel, camModelTf):
        self.imgPub = imgPub
        self.planePathPub = rospy.Publisher("dronePath", Path, queue_size=1)
        self.planePath = Path()
        self.planePath.header.frame_id = "map"
        self.camModel = camModel
        self.camModelTf = camModelTf

    def setCamIntrinsics(self, camIntrinsics):
        self.camModel = camIntrinsics

    def ros2opencv(self, img):
        cv_img_rgb = cvbridge.compressed_imgmsg_to_cv2(img, "rgb8")
        return cv_img_rgb

    def opencv2ros(self, img):
        ros_img = cvbridge.cv2_to_imgmsg(img, "rgb8")
        return ros_img

    def callbackDetections(self, imageMsg, detectionsMsg):
        ocv_img = self.ros2opencv(imageMsg)
        # visualize detections
        for oneDet in detectionsMsg.detections:
            box = BoundingBox2D_to_OpenCVBox(oneDet.bbox)
            cv2.rectangle(ocv_img, (box[0], box[1]), (box[2], box[3]), (255, 0, 0), 3)

        ros_img = self.opencv2ros(ocv_img)
        self.imgPub.publish(ros_img)

    def callbackTracks(self, imageMsg, tracksMsg):
        stamp = imageMsg.header.stamp

        ocv_img = self.ros2opencv(imageMsg)

        tf_listener.waitForTransform(
            self.camModelTf, "map", stamp, rospy.Duration(3)
        )  # wait until the needed transformation is ready

        for oneTrack in tracksMsg.tracks:

            if (
                oneTrack.state == Track.STATETYPE_VALIDATED
                or oneTrack.state == Track.STATETYPE_MEASURED_VALIDATED
            ):
                color = (0, 255, 0)
            else:
                color = (255, 0, 0)

            if (
                oneTrack.state == Track.STATETYPE_MEASURED
                or oneTrack.state == Track.STATETYPE_MEASURED_VALIDATED
            ):
                box = BoundingBox2D_to_OpenCVBox(oneTrack.Detection2D.bbox)
                cv2.rectangle(ocv_img, (box[0], box[1]), (box[2], box[3]), color, 3)

            track3DPosRos = oneTrack.localPositions[0]
            track3DPos = np.array([track3DPosRos.x, track3DPosRos.y, 0], np.float)
            trackId = str(oneTrack.id)
            lineWidth = 6

            track2DPos = np.uint16(
                imagePointTransformations.project3DPosToPic(
                    track3DPos, self.camModel, self.camModelTf, stamp
                )
            )

            crossLen = 10
            cv2.line(
                ocv_img,
                (track2DPos[0], track2DPos[1] - crossLen),
                (track2DPos[0], track2DPos[1] + crossLen),
                color,
                lineWidth,
            )
            cv2.line(
                ocv_img,
                (track2DPos[0] - crossLen, track2DPos[1]),
                (track2DPos[0] + crossLen, track2DPos[1]),
                color,
                lineWidth,
            )
            """
            frameName = "t" + trackId
            tf_broadcaster.sendTransform(track3DPos,
                             (0.0, 0.0, 0.0, 1.0),
                             stamp,
                             frameName,
                             "map")
            """

            font = cv2.FONT_HERSHEY_DUPLEX
            textpos = (track2DPos[0] + 10, track2DPos[1] + 10)
            fontScale = 2.0
            fontColor = color
            lineType = 2
            cv2.putText(
                ocv_img,
                trackId,  # "c" + str(oneTrack.trackingCount
                textpos,
                font,
                fontScale,
                fontColor,
                lineType,
            )

        ros_img = self.opencv2ros(ocv_img)
        self.imgPub.publish(ros_img)
