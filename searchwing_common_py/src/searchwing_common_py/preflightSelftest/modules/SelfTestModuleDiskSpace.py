import os
import shutil

from diagnostic_msgs.msg import DiagnosticStatus

from .SelfTestModuleBase import SelfTestModuleBase


class SelfTestModuleDiskSpace(SelfTestModuleBase):
    def __init__(self, path, minSpaceAbs):
        super().__init__()
        self.name = "DiskSpace"
        self.path = path
        self.minSpaceAbs = minSpaceAbs

    def run(self):
        try:
            if not os.path.exists(self.path):
                self.set_result(DiagnosticStatus.ERROR, self.path + " doesnt exist!")
                return

            total, used, free = shutil.disk_usage(self.path)
            free = free // (2**30)
            total = total // (2**30)
            if free >= self.minSpaceAbs:
                self.set_result(
                    DiagnosticStatus.OK,
                    str(self.minSpaceAbs)
                    + "GB Needed - Free "
                    + str(free)
                    + "/"
                    + str(total)
                    + "GB",
                )
            else:
                self.set_result(
                    DiagnosticStatus.ERROR,
                    str(self.minSpaceAbs)
                    + "GB Needed - Free "
                    + str(free)
                    + "/"
                    + str(total)
                    + "GB",
                )
        except Exception as e:
            self.set_result(DiagnosticStatus.ERROR, "unknown error: {}".format(e))
