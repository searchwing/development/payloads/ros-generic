from diagnostic_msgs.msg import DiagnosticStatus

from .SelfTestModuleBase import SelfTestModuleBase


class SelfTestModuleValueInFile(SelfTestModuleBase):
    def __init__(self, name, path, direction, warningTresh, errorTresh):
        super().__init__()
        self.name = name
        self.path = path
        self.warningTresh = warningTresh
        self.errorTresh = errorTresh
        self.direction = direction

    def run(self):
        try:
            with open(self.path, "r") as file:
                valueStr = file.read()
                value = float(valueStr)
                file.close()
                valueStrCropped = "{:.1f}".format(value)
            if self.direction == "<":
                if value < self.warningTresh:
                    self.set_result(
                        DiagnosticStatus.OK,
                        "Val:" + valueStrCropped + "<Warn:" + str(self.warningTresh),
                    )
                elif value < self.errorTresh:
                    self.set_result(
                        DiagnosticStatus.WARN,
                        "Val:" + valueStrCropped + "<Err:" + str(self.errorTresh),
                    )
                elif value >= self.errorTresh:
                    self.set_result(
                        DiagnosticStatus.ERROR,
                        "Val:" + valueStrCropped + ">Err:" + str(self.errorTresh),
                    )
            elif self.direction == ">":
                if value > self.warningTresh:
                    self.set_result(
                        DiagnosticStatus.OK,
                        "Val:" + valueStrCropped + ">Warn:" + str(self.warningTresh),
                    )
                elif value > self.errorTresh:
                    self.set_result(
                        DiagnosticStatus.WARN,
                        "Val:" + valueStrCropped + ">Err:" + str(self.errorTresh),
                    )
                elif value <= self.errorTresh:
                    self.set_result(
                        DiagnosticStatus.ERROR,
                        "Val:" + valueStrCropped + "<Err:" + str(self.errorTresh),
                    )
            else:
                self.set_result(
                    DiagnosticStatus.ERROR, "Unsupported direction to compare value to"
                )
        except Exception as e:
            self.set_result(DiagnosticStatus.ERROR, "unknown error: {}".format(e))
