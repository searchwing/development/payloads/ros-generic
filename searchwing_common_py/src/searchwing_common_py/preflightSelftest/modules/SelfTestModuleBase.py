import rospy
from diagnostic_msgs.msg import DiagnosticStatus

TEST_STATUS_STR_DECODE = {
    DiagnosticStatus.STALE: ("NOTRUN"),
    DiagnosticStatus.ERROR: ("ERROR"),
    DiagnosticStatus.WARN: ("WARN"),
    DiagnosticStatus.OK: ("OK"),
}


class SelfTestModuleBase:
    """
    Base class for SelfTestModules.

    Holds the name and the result, if any.

    """

    def __init__(self):
        self.name = "Base"

    def run(self):
        raise NotImplementedError("Please implement the run method!")

    def set_result(self, level, reason=""):
        self.result = DiagnosticStatus()
        self.result.message = reason
        self.result.level = level
        self.result.name = self.name

    def get_result(self):
        return self.result

    def print_result(self):
        if self.result.level is DiagnosticStatus.STALE:
            rospy.loginfo(
                "[PREFLIGHT-TEST][{}] {}: {}".format(
                    TEST_STATUS_STR_DECODE[self.result.level],
                    self.result.name,
                    self.result.message,
                )
            )
        elif self.result.level is DiagnosticStatus.ERROR:
            rospy.logerr(
                "[PREFLIGHT-TEST][{}] {}: {}".format(
                    TEST_STATUS_STR_DECODE[self.result.level],
                    self.result.name,
                    self.result.message,
                )
            )
        elif self.result.level is DiagnosticStatus.WARN:
            rospy.logwarn(
                "[PREFLIGHT-TEST][{}] {}: {}".format(
                    TEST_STATUS_STR_DECODE[self.result.level],
                    self.result.name,
                    self.result.message,
                )
            )
        elif self.result.level is DiagnosticStatus.OK:
            rospy.loginfo(
                "[PREFLIGHT-TEST][{}] {}: {}".format(
                    TEST_STATUS_STR_DECODE[self.result.level],
                    self.result.name,
                    self.result.message,
                )
            )
        else:
            rospy.logerr(
                "[PREFLIGHT-TEST][{}] {}: {}".format(
                    TEST_STATUS_STR_DECODE[self.result.level],
                    self.result.name,
                    self.result.message,
                )
            )
