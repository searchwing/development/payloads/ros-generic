import os

from diagnostic_msgs.msg import DiagnosticStatus

from .SelfTestModuleBase import SelfTestModuleBase


class SelfTestModuleCheckService(SelfTestModuleBase):
    def __init__(self, serviceName):
        super().__init__()
        self.name = "Service"
        self.serviceName = serviceName

    def run(self):
        query = "systemctl is-active --quiet " + self.serviceName
        serviceIsActive = not os.system(query)
        query = "systemctl is-enabled --quiet " + self.serviceName
        serviceIsEnabled = not os.system(query)
        if serviceIsActive and serviceIsEnabled:
            self.set_result(DiagnosticStatus.OK, self.serviceName + " running")
        elif serviceIsActive and not serviceIsEnabled:
            self.set_result(
                DiagnosticStatus.WARN, self.serviceName + " running-not enabled"
            )
        elif not serviceIsActive and serviceIsEnabled:
            self.set_result(
                DiagnosticStatus.WARN, self.serviceName + " enabled-not running"
            )
        elif not serviceIsActive:
            self.set_result(DiagnosticStatus.ERROR, self.serviceName + " not running")
        else:
            self.set_result(DiagnosticStatus.ERROR, self.serviceName + " not running")
