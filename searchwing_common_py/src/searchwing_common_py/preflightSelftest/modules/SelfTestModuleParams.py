import csv
import glob
import os
from socket import gethostname

import mavros
import rospy
from diagnostic_msgs.msg import DiagnosticStatus
from mavros_msgs.srv import ParamPull

from .SelfTestModuleBase import SelfTestModuleBase

mavros.set_namespace()

float_precision = 6


def to_numeric(x, float_precision=float_precision):
    return round(float(x), float_precision) if "." in x else int(x)


# reused code from http://docs.ros.org/en/latest-available/api/mavros/html/param_8py_source.html
class MavProxyParam:
    """Parse MavProxyParam param files"""

    class CSVDialect(csv.Dialect):
        delimiter = " "
        doublequote = False
        skipinitialspace = True
        lineterminator = "\r\n"
        quoting = csv.QUOTE_NONE

    def read(self, filepath):
        params = {}

        with open(filepath) as csvfile:

            for data in csv.reader(csvfile, self.CSVDialect):
                if data[0].startswith("#"):
                    continue  # skip comments

                if len(data) != 2:
                    raise ValueError("wrong field count")
                    # continue

                params[data[0].strip()] = to_numeric(data[1])

        return params


class QGroundControlParam:
    """Parse QGC param files"""

    class CSVDialect(csv.Dialect):
        delimiter = "\t"
        doublequote = False
        skipinitialspace = True
        lineterminator = "\n"
        quoting = csv.QUOTE_NONE

    def read(self, filepath):
        params = {}

        with open(filepath) as csvfile:

            for data in csv.reader(csvfile, self.CSVDialect):
                if data[0].startswith("#"):
                    continue  # skip comments

                if len(data) != 5:
                    raise ValueError("wrong field count")
                    # continue

                params[data[2].strip()] = to_numeric(data[3])

        return params


def get_drone_params(force_pull=False):
    try:
        pull = rospy.ServiceProxy(mavros.get_topic("param", "pull"), ParamPull)
        ret = pull(force_pull=force_pull)
    except rospy.ServiceException as ex:
        raise IOError(str(ex))

    if not ret.success:
        raise IOError("Request failed.")

    params = rospy.get_param(mavros.get_topic("param"))

    # round params
    params_rounded = {}
    for key in params:
        params_rounded[key] = round(params[key], float_precision)
    return params_rounded


class SelfTestModuleParams(SelfTestModuleBase):
    def __init__(self, name, paramsfiles, paramsBlackListPath):
        super().__init__()
        self.name = name
        self.paramsfiles = paramsfiles

        with open(paramsBlackListPath) as file:
            lines = file.readlines()
            self.paramsBlackList = []
            for oneLine in lines:
                param = oneLine.strip()
                if param[0] == "#":
                    continue
                self.paramsBlackList.append(param)

    def run(self):
        notFound = 0
        for oneFile in self.paramsfiles:
            # Search for params file
            if "*" in oneFile:
                foundFiles = glob.glob(oneFile)
                if len(foundFiles) > 0:
                    oneFile = foundFiles[0]
                    print("Searched for file and found {}".format(oneFile))
                else:
                    notFound = notFound + 1
                    continue
            if not os.path.exists(oneFile):
                self.set_result(
                    DiagnosticStatus.WARN, "File {} not found.".format(oneFile)
                )
                notFound = notFound + 1
                continue

            # Read in params
            if oneFile.endswith(".params"):
                paramIO = QGroundControlParam()
                factoryParams = paramIO.read(oneFile)
                break
            elif oneFile.endswith(".parm"):
                paramIO = MavProxyParam()
                factoryParams = paramIO.read(oneFile)
                break
            else:
                self.set_result(
                    DiagnosticStatus.WARN,
                    "Unknown param extension - only QGC '.params' and APM '.parm' files supported.",
                )
                return
        if notFound == len(self.paramsfiles):
            self.set_result(DiagnosticStatus.WARN, "No factory setting file found!")
            return

        try:
            droneParams = get_drone_params()
        except:
            self.set_result(DiagnosticStatus.WARN, "Cant read params from drone.")
            return

        try:
            diff = set(factoryParams.items()) - set(droneParams.items())
            hostname = gethostname()
            out = ""
            out = out + "PARAM_NAME\t:FACTORY|{}\n".format(hostname)
            nonBlacklistedParamFound = False
            for oneParam in sorted(diff):
                oneParamName = oneParam[0]
                if any(
                    blackListedParam in oneParamName
                    for blackListedParam in self.paramsBlackList
                ):
                    continue
                out = out + "{}\t:{}|{}\n".format(
                    oneParamName,
                    self.format_value(factoryParams[oneParamName]),
                    self.format_value(droneParams[oneParamName]),
                )
                nonBlacklistedParamFound = True
            if not nonBlacklistedParamFound:
                severity = DiagnosticStatus.OK
                out = out + "Parmeters: No factory difference found!"
            else:
                severity = DiagnosticStatus.OK
            self.set_result(severity, out)
        except Exception as e:
            self.set_result(DiagnosticStatus.WARN, "unknown error: {}".format(e))

    def format_value(self, value):
        if isinstance(value, float):
            valueStrCropped = "{:.2f}".format(value)
        else:
            valueStrCropped = str(value)
        return valueStrCropped
