import hashlib
import os
import time

import piexif
from diagnostic_msgs.msg import DiagnosticStatus

from .SelfTestModuleBase import SelfTestModuleBase


class SelfTestModuleCameraImages(SelfTestModuleBase):
    def __init__(self, name, imageFile, timeout):
        super().__init__()
        self.name = name
        self.imageFile = imageFile
        self.timeout = timeout

    def get_file_md5(self, filepath):
        hash_md5 = hashlib.md5()
        with open(filepath, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()

    def is_valid_exif(self, exif_data) -> bool:
        if not "GPS" in exif_data:
            return False
        if (
            exif_data["GPS"][piexif.GPSIFD.GPSLongitude] == ((0, 1), (0, 1), (0, 1))
            and exif_data["GPS"][piexif.GPSIFD.GPSLatitude] == ((0, 1), (0, 1), (0, 1))
            and exif_data["GPS"][piexif.GPSIFD.GPSAltitude] == (0, 1)
        ):
            return False
        return True

    def file_externally_open(self, file):
        try:
            os.rename(file, file)
            return False
        except OSError:  # file is in use
            return True

    def run(self):
        try:
            if not os.path.isfile(self.imageFile):
                self.set_result(DiagnosticStatus.ERROR, "can't take/save photos!")
                return

            start_hash = self.get_file_md5(self.imageFile)

            time.sleep(self.timeout)

            end_hash = self.get_file_md5(self.imageFile)

            while (
                self.file_externally_open(self.imageFile)
                or (os.stat(self.imageFile).st_size / (1024 * 1024)) < 0.3
            ):  # check if file smaller 0.5MB
                time.sleep(0.5)
            exif_dict = piexif.load(self.imageFile)

            # check if new image is recorded since start_stamp + timeout
            if start_hash != end_hash:
                if not self.is_valid_exif(exif_dict):
                    self.set_result(
                        DiagnosticStatus.ERROR, "images saved without GPS data!"
                    )
                else:
                    self.set_result(DiagnosticStatus.OK, "works as expected!")
            else:
                self.set_result(DiagnosticStatus.ERROR, "can't take/save photos!")
        except Exception as e:
            self.set_result(DiagnosticStatus.ERROR, "unknown error: {}".format(e))
