import rospy
from diagnostic_msgs.msg import DiagnosticStatus

from .SelfTestModuleBase import SelfTestModuleBase


class SelfTestModuleValueInMsg(SelfTestModuleBase):
    def __init__(
        self,
        name,
        topic,
        direction,
        warningTresh,
        errorTresh,
        unitString,
        datatype,
        timeout=4,
    ):
        super().__init__()
        self.name = name
        self.topic = topic
        self.datatype = datatype
        self.timeout = timeout
        self.warningTresh = warningTresh
        self.errorTresh = errorTresh
        self.direction = direction
        self.unitString = unitString

    def run(self):
        try:
            msg = rospy.wait_for_message(self.topic, self.datatype, self.timeout)
        except:
            self.set_result(DiagnosticStatus.ERROR, "No data from " + self.topic)
            return
        try:
            value = msg.data
            valueStrCropped = "{:.1f}".format(value)
            if self.direction == "<":
                if value <= self.warningTresh:
                    self.set_result(
                        DiagnosticStatus.OK,
                        valueStrCropped
                        + "<Warn:"
                        + str(self.warningTresh)
                        + self.unitString,
                    )
                elif value < self.errorTresh:
                    self.set_result(
                        DiagnosticStatus.WARN,
                        valueStrCropped
                        + "<Err:"
                        + str(self.errorTresh)
                        + self.unitString,
                    )
                elif value >= self.errorTresh:
                    self.set_result(
                        DiagnosticStatus.ERROR,
                        valueStrCropped
                        + ">Err:"
                        + str(self.errorTresh)
                        + self.unitString,
                    )
            elif self.direction == ">":
                if value > self.warningTresh:
                    self.set_result(
                        DiagnosticStatus.OK,
                        +valueStrCropped
                        + ">Warn:"
                        + str(self.warningTresh)
                        + self.unitString,
                    )
                elif value > self.errorTresh:
                    self.set_result(
                        DiagnosticStatus.WARN,
                        valueStrCropped
                        + ">Err:"
                        + str(self.errorTresh)
                        + self.unitString,
                    )
                elif value <= self.errorTresh:
                    self.set_result(
                        DiagnosticStatus.ERROR,
                        valueStrCropped
                        + "<Err:"
                        + str(self.errorTresh)
                        + self.unitString,
                    )
            else:
                self.set_result(
                    DiagnosticStatus.ERROR, "Unsupported direction to compare value to"
                )
        except Exception as e:
            self.set_result(DiagnosticStatus.ERROR, "unknown error: {}".format(e))
