import datetime

from diagnostic_msgs.msg import DiagnosticStatus

from .SelfTestModuleBase import SelfTestModuleBase


class SelfTestModuleClock(SelfTestModuleBase):
    def __init__(self):
        super().__init__()
        self.name = "Clock"

    def run(self):
        stamp = datetime.datetime.now()
        stampString = stamp.strftime("%Y-%m-%dT%H:%M:%S")
        if (
            int(stamp.strftime("%s")) < 1649183514
        ):  # Tuesday, April 5, 2022 8:31:54 PM GMT+02:00 DST
            self.set_result(DiagnosticStatus.WARN, "{}".format(stampString))
        else:
            self.set_result(DiagnosticStatus.OK, "{}".format(stampString))
