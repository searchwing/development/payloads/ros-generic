import rospy


class SelfTest:
    """ """

    def __init__(self, modules):
        self.result = None
        self._modules = modules

    def run_all(self):
        for module in self._modules:
            rospy.logdebug("[PREFLIGHT-TEST] Start module {}...".format(module.name))
            module.run()

    def run(self, module_name):
        pass

    def print_result(self):
        for module in self._modules:
            module.print_result()

    def save_result(self, file_path):
        pass

    def get_results_string(self):
        results = []
        for module in self._modules:
            results.append(
                {"SelfTestModule": module.name, "Result": module.result.to_json()}
            )
        return results

    def get_results_diagnostics(self):
        results = []
        for module in self._modules:
            results.append(module.get_result())
        return results
