from datetime import datetime

import cv2
import numpy as np
import piexif
from cv_bridge import CvBridge
from sensor_msgs.msg import CompressedImage, Image

cv_bridge = CvBridge()


def BoundingBox2D_to_OpenCVBox(BoundingBox2D):
    xmin = int(BoundingBox2D.center.x - BoundingBox2D.size_x / 2.0)
    ymin = int(BoundingBox2D.center.y - BoundingBox2D.size_y / 2.0)
    xmax = int(BoundingBox2D.center.x + BoundingBox2D.size_x / 2.0)
    ymax = int(BoundingBox2D.center.y + BoundingBox2D.size_y / 2.0)
    return np.array([xmin, ymin, xmax, ymax], int)


def rosImgMsg_to_opencvImg(rosImgMsg):
    if isinstance(rosImgMsg, CompressedImage):
        np_arr = np.fromstring(rosImgMsg.data, np.uint8)
        frame = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
    elif isinstance(rosImgMsg, Image):
        # Get image from message
        frame = cv_bridge.imgmsg_to_cv2(rosImgMsg)  # , 'bgr8')
    else:
        print("Unknown image type.")
    return frame


def exif_timestamp_to_datetime(exif_dict):
    encoding = "utf-8"
    return datetime.strptime(
        str(exif_dict["Exif"][piexif.ExifIFD.DateTimeOriginal], encoding)
        + "."
        + str(exif_dict["Exif"][piexif.ExifIFD.SubSecTimeOriginal], encoding),
        "%Y:%m:%d %H:%M:%S.%f",
    )
