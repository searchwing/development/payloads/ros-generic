from datetime import datetime
from socket import gethostname

import rospy


class MissionManager:
    def __init__(self):
        self.isArmed = None

        self.hostname = gethostname()
        self.currentMissionName = ""
        self.noMissionName = "noMission-" + self.hostname

    def processIsArmed(self, newIsArmed):

        # EVENT: Arming / Takeoff
        if newIsArmed and (not self.isArmed or self.isArmed is None):
            rospy.loginfo("[MISSION_MANAGER] EVENT: Arming / Takeoff detected!")
            missionStartStamp = datetime.now()
            missionStartStampString = missionStartStamp.strftime("%Y-%m-%dT%H-%M-%S")
            self.currentMissionName = missionStartStampString + "-" + self.hostname
            rospy.loginfo(
                "[MISSION_MANAGER] New mission created: " + self.currentMissionName
            )

        # EVENT: Disarm / Landing
        elif not newIsArmed and (self.isArmed or self.isArmed is None):
            rospy.loginfo("[MISSION_MANAGER] EVENT: Disarm / Landing detected!")
            self.currentMissionName = self.noMissionName

        self.isArmed = newIsArmed
        return self.currentMissionName
