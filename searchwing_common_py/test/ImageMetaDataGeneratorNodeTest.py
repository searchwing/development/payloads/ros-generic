#!/usr/bin/env python

import math
import time
import unittest
from socket import gethostname

import numpy as np
import rospy
import rostest
import tf
from geometry_msgs.msg import Point
from sensor_msgs.msg import CameraInfo, NavSatFix
from std_msgs.msg import Bool, Float64, Header, String

from searchwing_msgs.msg import DroneState, ImageMetadata


class ImageMetaDataGeneratorNodeTestCase(unittest.TestCase):
    def setUp(self):
        rospy.init_node("ImageMetaDataGeneratorNodeTest", anonymous=True)
        rospy.Subscriber(
            "/camera/imageMetadata", ImageMetadata, self.callbackImageMetaData
        )
        self.tf_listener = tf.TransformListener()
        self._data = None
        self.timeoutInSec = 5
        self.hostname = gethostname()

    def createCameraInfoMessageAndPub(self, stamp):
        camera_infoPub = rospy.Publisher(
            "/camera/camera_info", CameraInfo, queue_size=2
        )
        camera_info = CameraInfo()
        camera_info.header = Header()
        camera_info.header.stamp = stamp
        camera_pixel_width = 100
        camera_pixel_height = 100
        camera_sensor_size_width = 2.0
        camera_sensor_size_height = 2.0
        camera_focal_length = 1.0
        camera_pixel_focal_length_x = (
            camera_focal_length * camera_pixel_width / camera_sensor_size_width
        )
        camera_pixel_focal_length_y = (
            camera_focal_length * camera_pixel_height / camera_sensor_size_height
        )
        camera_info.K = [
            camera_pixel_focal_length_x,
            0.0,
            camera_pixel_width / 2.0,
            0.0,
            camera_pixel_focal_length_y,
            camera_pixel_height / 2.0,
            0.0,
            0.0,
            1.0,
        ]
        camera_info.P = [
            camera_pixel_focal_length_x,
            0.0,
            camera_pixel_width / 2.0,
            0.0,
            0.0,
            camera_pixel_focal_length_y,
            camera_pixel_height / 2.0,
            0.0,
            0.0,
            0.0,
            1.0,
            0.0,
        ]
        camera_info.width = camera_pixel_width
        camera_info.height = camera_pixel_height
        return camera_infoPub, camera_info

    def createDroneStateMessageAndPub(self, stamp, isFlying, missionName):
        droneStatePub = rospy.Publisher("/searchwing/state", DroneState, queue_size=2)
        droneStateMsg = DroneState()
        droneStateMsg.header = Header()
        droneStateMsg.header.stamp = stamp
        droneStateMsg.isFlying = Bool(isFlying)
        droneStateMsg.missionName = String(missionName)
        return droneStatePub, droneStateMsg

    def createCompassMessageAndPub(self):
        compassPub = rospy.Publisher(
            "/mavros/global_position/compass_hdg", Float64, queue_size=2
        )
        compassMsg = Float64()
        compassMsg.data = np.pi
        return compassPub, compassMsg

    def createNavSatFixMessageAndPub(self, stamp):
        NavSatFixPub = rospy.Publisher(
            "/mavros/global_position/global", NavSatFix, queue_size=2
        )
        NavSatFixMsg = NavSatFix()
        NavSatFixMsg.header.stamp = stamp
        NavSatFixMsg.latitude = 52.523464841353935
        NavSatFixMsg.longitude = 13.420196144969186
        NavSatFixMsg.altitude = (
            200.0  # fly 200 above sea level, but 100 at local "map" frame
        )
        return NavSatFixPub, NavSatFixMsg

    def publishTransforms(self, stamp, rollangle=0.0):
        cameraTfName = "cam"
        localPoseAltitude = 100.0
        br = tf.TransformBroadcaster()
        time.sleep(0.5)
        br.sendTransform(
            (0, 0, localPoseAltitude),
            tf.transformations.quaternion_from_euler(rollangle, 0, 0),
            stamp,
            "base_link",
            "map",
        )
        time.sleep(0.5)
        br.sendTransform(
            (0, 0, 0),
            tf.transformations.quaternion_from_euler(0, np.pi / 2.0, 0),
            stamp,
            "cam_base",
            "base_link",
        )
        time.sleep(0.5)
        br.sendTransform(
            (0, 0, 0),
            tf.transformations.quaternion_from_euler(-np.pi / 2.0, 0, -np.pi / 2.0),
            stamp,
            cameraTfName,
            "cam_base",
        )
        time.sleep(1.0)

        self.tf_listener.waitForTransform(
            "map",
            cameraTfName,
            stamp,
            rospy.Duration(3),  # TODO use cam tf from camera info
        )
        self.localPos = Point()
        self.localPos.x = 0
        self.localPos.y = 0
        self.localPos.z = localPoseAltitude

    def test_imageMetadataGemeratorNode_0OnlyCamInfo(self):
        # should be run as first test, as imageMetadataGemeratorNode is not State-less and saves last data from inputs
        stamp = rospy.Time.now()

        self.publishTransforms(stamp)

        cameraInfoPub, cameraInfo = self.createCameraInfoMessageAndPub(stamp)
        time.sleep(1.0)
        cameraInfoPub.publish(cameraInfo)

        timeout = time.time() + self.timeoutInSec
        while not rospy.core.is_shutdown() and self._data is None:
            rospy.rostime.wallsleep(1.5)
            if time.time() >= timeout:
                raise rospy.exceptions.ROSException(
                    "timeout exceeded while waiting for imagemetadatamsg "
                )

        self.assertEqual(len(self._data.imageCornerGpsPos), 5)
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UL].lat, 0.0
        )
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UL].lon, 0.0
        )
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UR].lat, 0.0
        )
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UR].lon, 0.0
        )
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LR].lat, 0.0
        )
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LR].lon, 0.0
        )
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LL].lat, 0.0
        )
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LL].lon, 0.0
        )
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_CE].lat, 0.0
        )
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_CE].lon, 0.0
        )

        self.assertAlmostEqual(self._data.droneGpsPos.latitude, 0.0)
        self.assertAlmostEqual(self._data.droneGpsPos.longitude, 0.0)
        self.assertAlmostEqual(self._data.heading, 0.0)
        self.assertIsNotNone(self._data.imageName.data)
        self.assertEqual(self._data.imageName.data[-4:], ".jpg")
        self.assertEqual(self._data.sensorName.data, "cam")
        self.assertEqual(self._data.droneState.isFlying.data, True)
        self.assertEqual(
            self._data.droneState.missionName.data, "noMission-" + self.hostname
        )

    def test_imageMetadataGemeratorNode_1Full(self):
        stamp = rospy.Time.now()

        self.publishTransforms(stamp)

        droneStatePub, droneStateMsg = self.createDroneStateMessageAndPub(
            stamp, True, "aaaa"
        )
        compassPub, compass = self.createCompassMessageAndPub()
        navSatFixPub, navSatFix = self.createNavSatFixMessageAndPub(stamp)
        cameraInfoPub, cameraInfo = self.createCameraInfoMessageAndPub(stamp)
        time.sleep(1.0)
        droneStatePub.publish(droneStateMsg)
        compassPub.publish(compass)
        navSatFixPub.publish(navSatFix)
        time.sleep(1.0)
        cameraInfoPub.publish(cameraInfo)

        timeout = time.time() + self.timeoutInSec
        while not rospy.core.is_shutdown() and self._data is None:
            rospy.rostime.wallsleep(1.5)
            if time.time() >= timeout:
                raise rospy.exceptions.ROSException(
                    "timeout exceeded while waiting for imagemetadatamsg "
                )

        self.assertEqual(len(self._data.imageCornerGpsPos), 5)
        # Drone YAW == 0° => Drone is heading East due to ROS ENU conventions => Camera Upper boarder is looking towards East
        # UL is in NorthEast direction
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UL].lat,
            52.52436219443437,
            places=5,
        )
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UL].lon,
            13.421674571966859,
            places=5,
        )
        # UR is in SouthEast direction
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UR].lat,
            52.522565123424584,
            places=5,
        )
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UR].lon,
            13.421674571966859,
            places=5,
        )
        # LR is in SouthWest direction
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LR].lat,
            52.522565123424584,
            places=5,
        )
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LR].lon,
            13.41871806,
            places=5,
        )
        # LL is in NorthWest direction
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LL].lat,
            52.52436219443437,
            places=5,
        )  # 5 digits == ~1.1m  accuarcy
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LL].lon,
            13.41871806,
            places=5,
        )

        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_CE].lat,
            navSatFix.latitude,
            places=5,
        )
        self.assertAlmostEqual(
            self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_CE].lon,
            navSatFix.longitude,
            places=5,
        )

        self.assertEqual(self._data.droneGpsPos, navSatFix)
        self.assertEqual(self._data.droneLocalPose.position, self.localPos)
        self.assertEqual(self._data.heading, compass.data)
        self.assertIsNotNone(self._data.imageName.data)
        self.assertEqual(self._data.sensorName.data, "cam")
        self.assertEqual(self._data.droneState, droneStateMsg)

    def test_imageMetadataGemeratorNode_2FullHorizonInImage(self):
        stamp = rospy.Time.now()

        self.publishTransforms(
            stamp, np.pi / 2
        )  # roll drone to the right , cam looks to the left

        droneStatePub, droneStateMsg = self.createDroneStateMessageAndPub(
            stamp, True, "aaaa"
        )
        compassPub, compass = self.createCompassMessageAndPub()
        navSatFixPub, navSatFix = self.createNavSatFixMessageAndPub(stamp)
        cameraInfoPub, cameraInfo = self.createCameraInfoMessageAndPub(stamp)
        time.sleep(1.0)
        droneStatePub.publish(droneStateMsg)
        compassPub.publish(compass)
        navSatFixPub.publish(navSatFix)
        time.sleep(1.0)
        cameraInfoPub.publish(cameraInfo)

        timeout = time.time() + self.timeoutInSec
        while not rospy.core.is_shutdown() and self._data is None:
            rospy.rostime.wallsleep(1.5)
            if time.time() >= timeout:
                raise rospy.exceptions.ROSException(
                    "timeout exceeded while waiting for imagemetadatamsg "
                )

        self.assertEqual(len(self._data.imageCornerGpsPos), 5)
        # as cam looks above the horizon to the left, UL and LL should be invalid
        self.assertTrue(
            math.isnan(self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UL].lat)
        )
        self.assertTrue(
            math.isnan(self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UL].lon)
        )
        self.assertTrue(self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UR].lat)
        self.assertTrue(self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UR].lon)
        self.assertTrue(self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LR].lat)
        self.assertTrue(self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LR].lon)
        self.assertTrue(
            math.isnan(self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LL].lat)
        )
        self.assertTrue(
            math.isnan(self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LL].lon)
        )
        self.assertTrue(
            math.isnan(self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_CE].lat)
        )
        self.assertTrue(
            math.isnan(self._data.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_CE].lon)
        )

        self.assertTrue(
            math.isnan(self._data.imageCornerGSD[ImageMetadata.GPS_POS_IDX_UL])
        )
        self.assertTrue(self._data.imageCornerGSD[ImageMetadata.GPS_POS_IDX_UR])
        self.assertTrue(self._data.imageCornerGSD[ImageMetadata.GPS_POS_IDX_LR])
        self.assertTrue(
            math.isnan(self._data.imageCornerGSD[ImageMetadata.GPS_POS_IDX_LL])
        )
        self.assertTrue(
            math.isnan(self._data.imageCornerGSD[ImageMetadata.GPS_POS_IDX_CE])
        )

        self.assertEqual(self._data.droneGpsPos, navSatFix)
        self.assertEqual(self._data.droneLocalPose.position, self.localPos)
        self.assertEqual(self._data.heading, compass.data)
        self.assertIsNotNone(self._data.imageName.data)
        self.assertEqual(self._data.sensorName.data, "cam")
        self.assertEqual(self._data.droneState, droneStateMsg)

    def callbackImageMetaData(self, imageMetaData):
        self._data = imageMetaData

    def tearDown(self) -> None:
        self._data = None


if __name__ == "__main__":
    rostest.rosrun(
        "searchwing_common_py",
        "ImageMetaDataGeneratorNodeTest",
        ImageMetaDataGeneratorNodeTestCase,
    )
