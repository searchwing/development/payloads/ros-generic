#!/usr/bin/env python

import math
import os
import shutil
import time
import unittest
from socket import gethostname

import cv2
import numpy as np
import piexif
import rospy
import rostest
from geometry_msgs.msg import Pose
from sensor_msgs.msg import CompressedImage, NavSatFix
from std_msgs.msg import Bool, Header, String

from searchwing_msgs.msg import GpsPos, ImageMetadata


class ImageSaverNodeTestCase(unittest.TestCase):
    timeoutInSec = 2
    hostname = gethostname()
    maxDiff = None

    def setUp(self):
        blank_image = np.zeros((2464, 3280, 3), np.uint8)
        encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 95]
        result, blank_image_encoded = cv2.imencode(".jpg", blank_image, encode_param)
        self.dummyImg = blank_image_encoded.tobytes()
        time.sleep(0.2)

    def deleteDir(self, folder):
        if not os.path.isdir(folder):
            return

        for filename in os.listdir(folder):
            file_path = os.path.join(folder, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print("Failed to delete %s. Reason: %s" % (file_path, e))

    def arrange(self):
        self.deleteDir("/tmp/searchwing")
        os.makedirs("/tmp/searchwing", exist_ok=True)
        rospy.init_node("ImageSaverNodeTest", anonymous=True)
        stamp = rospy.Time.now()
        return stamp

    def createImageMessageAndPub(self, stamp):
        print("createImageMessageAndPub: " + str(stamp))
        imagePub = rospy.Publisher(
            "/camera/image/compressed", CompressedImage, queue_size=2
        )
        image = CompressedImage()
        image.header = Header()
        image.header.stamp = stamp
        image.data = self.dummyImg
        return imagePub, image

    def createMetaMessageAndPub(self, stamp, missionName, isFlying, altitude=100.0):
        print("createMetaMessageAndPub: " + str(stamp))
        metaPub = rospy.Publisher("/camera/imageMetadata", ImageMetadata, queue_size=2)
        meta = ImageMetadata()
        meta.header = Header()
        meta.header.stamp = stamp
        meta.imageName = String("testimage.jpg")
        meta.sensorName = String("cam-l")
        meta.imageCornerGpsPos = [None] * 5
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_CE] = GpsPos()
        meta.imageCornerGpsPos[
            ImageMetadata.GPS_POS_IDX_CE
        ].lat = ImageMetadata.GPS_POS_IDX_CE
        meta.imageCornerGpsPos[
            ImageMetadata.GPS_POS_IDX_CE
        ].lon = ImageMetadata.GPS_POS_IDX_CE
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LL] = GpsPos()
        meta.imageCornerGpsPos[
            ImageMetadata.GPS_POS_IDX_LL
        ].lat = ImageMetadata.GPS_POS_IDX_LL
        meta.imageCornerGpsPos[
            ImageMetadata.GPS_POS_IDX_LL
        ].lon = ImageMetadata.GPS_POS_IDX_LL
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LR] = GpsPos()
        meta.imageCornerGpsPos[
            ImageMetadata.GPS_POS_IDX_LR
        ].lat = ImageMetadata.GPS_POS_IDX_LR
        meta.imageCornerGpsPos[
            ImageMetadata.GPS_POS_IDX_LR
        ].lon = ImageMetadata.GPS_POS_IDX_LR
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UL] = GpsPos()
        meta.imageCornerGpsPos[
            ImageMetadata.GPS_POS_IDX_UL
        ].lat = ImageMetadata.GPS_POS_IDX_UL
        meta.imageCornerGpsPos[
            ImageMetadata.GPS_POS_IDX_UL
        ].lon = ImageMetadata.GPS_POS_IDX_UL
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UR] = GpsPos()
        meta.imageCornerGpsPos[
            ImageMetadata.GPS_POS_IDX_UR
        ].lat = ImageMetadata.GPS_POS_IDX_UR
        meta.imageCornerGpsPos[
            ImageMetadata.GPS_POS_IDX_UR
        ].lon = ImageMetadata.GPS_POS_IDX_UR
        meta.imageCornerGSD = [0.0] * 5
        meta.imageCornerGSD[ImageMetadata.GPS_POS_IDX_CE] = ImageMetadata.GPS_POS_IDX_CE
        meta.imageCornerGSD[ImageMetadata.GPS_POS_IDX_UL] = ImageMetadata.GPS_POS_IDX_UL
        meta.imageCornerGSD[ImageMetadata.GPS_POS_IDX_UR] = ImageMetadata.GPS_POS_IDX_UR
        meta.imageCornerGSD[ImageMetadata.GPS_POS_IDX_LR] = ImageMetadata.GPS_POS_IDX_LR
        meta.imageCornerGSD[ImageMetadata.GPS_POS_IDX_LL] = ImageMetadata.GPS_POS_IDX_LL
        meta.heading = 0.4
        meta.droneGpsPos = NavSatFix()
        meta.droneGpsPos.latitude = 3.2
        meta.droneGpsPos.longitude = 1.2
        meta.droneGpsPos.altitude = 200.0
        meta.droneLocalPose = Pose()
        meta.droneLocalPose.position.x = 0
        meta.droneLocalPose.position.y = 0
        meta.droneLocalPose.position.z = altitude
        meta.droneState.header = Header()
        meta.droneState.header.stamp = stamp
        meta.droneState.isFlying = Bool(isFlying)
        meta.droneState.missionName = String(missionName)
        return metaPub, meta

    def createMetaMessageWithInvalidAndPub(self, stamp, missionName, isFlying):
        print("createMetaMessageAndPub: " + str(stamp))
        metaPub = rospy.Publisher("/camera/imageMetadata", ImageMetadata, queue_size=2)
        meta = ImageMetadata()
        meta.header = Header()
        meta.header.stamp = stamp
        meta.imageName = String("testimage.jpg")
        meta.sensorName = String("cam-l")
        meta.imageCornerGpsPos = [None] * 5
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_CE] = GpsPos()
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_CE].lat = math.nan
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_CE].lon = math.nan
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LL] = GpsPos()
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LL].lat = math.nan
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LL].lon = math.nan
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LR] = GpsPos()
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LR].lat = math.nan
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_LR].lon = math.nan
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UL] = GpsPos()
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UL].lat = math.nan
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UL].lon = math.nan
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UR] = GpsPos()
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UR].lat = math.nan
        meta.imageCornerGpsPos[ImageMetadata.GPS_POS_IDX_UR].lon = math.nan
        meta.imageCornerGSD = [0.0] * 5
        meta.imageCornerGSD[ImageMetadata.GPS_POS_IDX_CE] = math.nan
        meta.imageCornerGSD[ImageMetadata.GPS_POS_IDX_UL] = math.nan
        meta.imageCornerGSD[ImageMetadata.GPS_POS_IDX_UR] = math.nan
        meta.imageCornerGSD[ImageMetadata.GPS_POS_IDX_LR] = math.nan
        meta.imageCornerGSD[ImageMetadata.GPS_POS_IDX_LL] = math.nan
        meta.heading = 0.4
        meta.droneGpsPos = NavSatFix()
        meta.droneGpsPos.latitude = 3.2
        meta.droneGpsPos.longitude = 1.2
        meta.droneGpsPos.altitude = -200.0
        meta.droneLocalPose = Pose()
        meta.droneLocalPose.position.x = 0
        meta.droneLocalPose.position.y = 0
        meta.droneLocalPose.position.z = -100.0
        meta.droneState.header = Header()
        meta.droneState.header.stamp = stamp
        meta.droneState.isFlying = Bool(isFlying)
        meta.droneState.missionName = String(missionName)
        return metaPub, meta

    def test_no_mission_not_flying(self):
        expectedPath = "/tmp/searchwing/noMission/cam-l/preflight.jpg"
        # arrange
        stamp = self.arrange()

        imagePub, image = self.createImageMessageAndPub(stamp)
        metaPub, meta = self.createMetaMessageAndPub(stamp, "noMission", False)

        timeout = time.time() + self.timeoutInSec
        exists = os.path.isfile(expectedPath)
        while not rospy.is_shutdown() and not exists and time.time() < timeout:
            # act
            imagePub.publish(image)
            metaPub.publish(meta)

            time.sleep(0.5)
            exists = os.path.isfile(expectedPath)

        # assert
        self.assertTrue(exists, "image testimage.jpg not found under " + expectedPath)
        self.assertExif(expectedPath)

    def test_mission_not_flying(self):
        expectedPath = "/tmp/searchwing/missionA/cam-l/preflight.jpg"

        # arrange
        stamp = self.arrange()

        imagePub, image = self.createImageMessageAndPub(stamp)
        metaPub, meta = self.createMetaMessageAndPub(stamp, "missionA", False)

        timeout = time.time() + self.timeoutInSec
        exists = os.path.isfile(expectedPath)
        while not rospy.is_shutdown() and not exists and time.time() < timeout:
            # act
            imagePub.publish(image)
            metaPub.publish(meta)

            time.sleep(0.5)
            exists = os.path.isfile(expectedPath)

        # assert
        self.assertTrue(exists, "image testimage.jpg not found under " + expectedPath)
        self.assertExif(expectedPath)

    def test_no_mission_flying(self):
        expectedPath = "/tmp/searchwing/noMission/cam-l/testimage.jpg"
        # arrange
        stamp = self.arrange()

        imagePub, image = self.createImageMessageAndPub(stamp)
        metaPub, meta = self.createMetaMessageAndPub(stamp, "noMission", True)

        timeout = time.time() + self.timeoutInSec
        exists = os.path.isfile(expectedPath)
        while not rospy.is_shutdown() and not exists and time.time() < timeout:
            # act
            imagePub.publish(image)
            metaPub.publish(meta)

            time.sleep(0.5)
            exists = os.path.isfile(expectedPath)

        # assert
        self.assertTrue(exists, "image testimage.jpg not found under " + expectedPath)
        self.assertExif(expectedPath)

    def assertExif(self, expectedPath):
        exif_dict = piexif.load(expectedPath)

        self.assertEquals(exif_dict["GPS"][piexif.GPSIFD.GPSLatitudeRef], b"N")
        self.assertEquals(
            exif_dict["GPS"][piexif.GPSIFD.GPSLatitude], ((3, 1), (12, 1), (0, 1))
        )
        self.assertEquals(exif_dict["GPS"][piexif.GPSIFD.GPSLongitudeRef], b"E")
        self.assertEquals(
            exif_dict["GPS"][piexif.GPSIFD.GPSLongitude], ((1, 1), (11, 1), (60, 1))
        )
        self.assertEquals(exif_dict["GPS"][piexif.GPSIFD.GPSAltitudeRef], 0)
        self.assertEquals(exif_dict["GPS"][piexif.GPSIFD.GPSAltitude], (100, 1))
        self.assertEquals(
            exif_dict["GPS"][piexif.GPSIFD.GPSLongitude], ((1, 1), (11, 1), (60, 1))
        )
        self.assertEquals(exif_dict["GPS"][piexif.GPSIFD.GPSImgDirection], (2, 5))
        self.assertEquals(exif_dict["GPS"][piexif.GPSIFD.GPSImgDirectionRef], b"T")

        jsonString = '{"imgcrns": {"ul": "0.0,0.0,0.0", "ur": "1.0,1.0,0.0", "lr": "2.0,2.0,0.0", "ll": "3.0,3.0,0.0"}, "imgcrnsGSD": {"ul": "0.0", "ur": "1.0", "lr": "2.0", "ll": "3.0"}}'
        # removing leading 'ASCII\x00\x00\x00' bytes according to https://piexif.readthedocs.io/en/latest/helper.html#piexif.helper.UserComment.dump
        exifComment = exif_dict["Exif"][piexif.ExifIFD.UserComment].decode("utf-8")[8:]
        self.assertEquals(exifComment, jsonString)

        self.assertEquals(
            exif_dict["0th"][piexif.ImageIFD.Artist].decode("utf-8"),
            self.hostname + "-cam-l",
        )

        return exif_dict

    def assertExifInvalidData(self, expectedPath):
        exif_dict = piexif.load(expectedPath)

        self.assertEquals(exif_dict["GPS"][piexif.GPSIFD.GPSLatitudeRef], b"N")
        self.assertEquals(
            exif_dict["GPS"][piexif.GPSIFD.GPSLatitude], ((3, 1), (12, 1), (0, 1))
        )
        self.assertEquals(exif_dict["GPS"][piexif.GPSIFD.GPSLongitudeRef], b"E")
        self.assertEquals(
            exif_dict["GPS"][piexif.GPSIFD.GPSLongitude], ((1, 1), (11, 1), (60, 1))
        )
        self.assertEquals(exif_dict["GPS"][piexif.GPSIFD.GPSAltitudeRef], 0)
        self.assertEquals(
            exif_dict["GPS"][piexif.GPSIFD.GPSAltitude], (0, 1)
        )  # check if negative altitude from input is set to 0
        self.assertEquals(exif_dict["GPS"][piexif.GPSIFD.GPSImgDirection], (2, 5))
        self.assertEquals(exif_dict["GPS"][piexif.GPSIFD.GPSImgDirectionRef], b"T")

        jsonString = '{"imgcrns": {"ul": "nan,nan,0.0", "ur": "nan,nan,0.0", "lr": "nan,nan,0.0", "ll": "nan,nan,0.0"}, "imgcrnsGSD": {"ul": "nan", "ur": "nan", "lr": "nan", "ll": "nan"}}'
        # removing leading 'ASCII\x00\x00\x00' bytes according to https://piexif.readthedocs.io/en/latest/helper.html#piexif.helper.UserComment.dump
        exifComment = exif_dict["Exif"][piexif.ExifIFD.UserComment].decode("utf-8")[8:]
        self.assertEquals(exifComment, jsonString)

        self.assertEquals(
            exif_dict["0th"][piexif.ImageIFD.Artist].decode("utf-8"),
            self.hostname + "-cam-l",
        )

        return exif_dict

    def test_mission_flying(self):
        expectedPath = "/tmp/searchwing/missionA/cam-l/testimage.jpg"
        # arrange
        stamp = self.arrange()

        imagePub, image = self.createImageMessageAndPub(stamp)
        metaPub, meta = self.createMetaMessageAndPub(stamp, "missionA", True)

        timeout = time.time() + self.timeoutInSec
        exists = os.path.isfile(expectedPath)
        while not rospy.is_shutdown() and not exists and time.time() < timeout:
            # act
            imagePub.publish(image)
            metaPub.publish(meta)
            time.sleep(0.5)
            exists = os.path.isfile(expectedPath)

        # assert
        self.assertTrue(exists, "image testimage.jpg not found under " + expectedPath)
        self.assertExif(expectedPath)

    def test_mission_flying_lowAlt(self):
        expectedPath = "/tmp/searchwing/missionA/cam-l/testimage.jpg"
        # arrange
        stamp = self.arrange()

        imagePub, image = self.createImageMessageAndPub(stamp)
        metaPub, meta = self.createMetaMessageAndPub(stamp, "missionA", True, 10)

        timeout = time.time() + self.timeoutInSec
        exists = os.path.isfile(expectedPath)
        while not rospy.is_shutdown() and not exists and time.time() < timeout:
            # act
            imagePub.publish(image)
            metaPub.publish(meta)
            time.sleep(0.5)
            exists = os.path.isfile(expectedPath)

        # assert
        self.assertFalse(exists, "image testimage.jpg found under " + expectedPath)

    def test_mission_flyingInvalidData(self):
        expectedPath = "/tmp/searchwing/missionA/cam-l/testimage.jpg"
        # arrange
        stamp = self.arrange()

        imagePub, image = self.createImageMessageAndPub(stamp)
        metaPub, meta = self.createMetaMessageWithInvalidAndPub(stamp, "missionA", True)

        timeout = time.time() + self.timeoutInSec
        exists = os.path.isfile(expectedPath)
        while not rospy.is_shutdown() and not exists and time.time() < timeout:
            # act
            imagePub.publish(image)
            metaPub.publish(meta)
            time.sleep(0.5)
            exists = os.path.isfile(expectedPath)

        # assert
        self.assertTrue(exists, "image testimage.jpg not found under " + expectedPath)
        self.assertExifInvalidData(expectedPath)


if __name__ == "__main__":
    rostest.rosrun("searchwing_common_py", "ImageSaverNodeTest", ImageSaverNodeTestCase)
