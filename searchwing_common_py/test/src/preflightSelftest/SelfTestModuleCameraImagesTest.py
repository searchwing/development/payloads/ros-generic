#!/usr/bin/env python

import os
import time
import unittest

import rospy
import rostest
from diagnostic_msgs.msg import DiagnosticStatus

from searchwing_common_py.preflightSelftest.modules.SelfTestModuleCameraImages import (
    SelfTestModuleCameraImages,
)


class SelfTestModuleCameraImagesTestCase(unittest.TestCase):
    def setUp(self):
        rospy.init_node("SelfTestModuleCameraImagesTestNode", anonymous=True)
        time.sleep(3)

        self.cameraSelftestTimeoutOffset = rospy.get_param(
            "/PreflightSelftestNode/i_cameraSelftestTimeoutOffset"
        )
        self.cameraSelftestPeriodTimeL = rospy.get_param(
            "/picamera_caml/i_imgPeriodTime"
        )
        self.cameraSelftestPeriodTimeR = rospy.get_param(
            "/picamera_camr/i_imgPeriodTime"
        )

        basepath = "/data/bilder/"
        self.camRpath = os.path.join(basepath, "cam-r_latest.jpg")
        self.camLpath = os.path.join(basepath, "cam-l_latest.jpg")

    def test_Selftest_camera_left(self):
        test = SelfTestModuleCameraImages(
            "test_Selftest_camera_left",
            self.camLpath,
            self.cameraSelftestPeriodTimeL + self.cameraSelftestTimeoutOffset,
        )
        test.run()
        self.assertEqual(test.get_result().level, DiagnosticStatus.OK)

    def test_Selftest_camera_right(self):
        test = SelfTestModuleCameraImages(
            "test_Selftest_camera_right",
            self.camRpath,
            self.cameraSelftestPeriodTimeR + self.cameraSelftestTimeoutOffset,
        )
        test.run()
        self.assertEqual(test.get_result().level, DiagnosticStatus.OK)

    def tearDown(self) -> None:
        self._data = None


if __name__ == "__main__":
    rostest.rosrun(
        "searchwing_common_py",
        "SelfTestModuleCameraImagesTest",
        SelfTestModuleCameraImagesTestCase,
    )
