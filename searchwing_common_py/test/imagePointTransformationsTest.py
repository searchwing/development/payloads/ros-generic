#!/usr/bin/env python

import time
import unittest

import numpy as np
import rospy
import rostest
import tf
from geometry_msgs.msg import PointStamped
from image_geometry import cameramodels
from sensor_msgs.msg import CameraInfo, NavSatFix
from std_msgs.msg import Header


class ImagePointTransformationsTestCase(unittest.TestCase):
    def setUp(self):
        rospy.init_node("ImagePointTransformationsTestNode", anonymous=True)
        self.tf_listener = tf.TransformListener()

        self.localGpsPos = NavSatFix()
        self.localGpsPos.latitude = 52.523464841353935
        self.localGpsPos.longitude = 13.420196144969186
        self.camModel = cameramodels.PinholeCameraModel()
        self.camModel.fromCameraInfo(self.createCameraInfo())

        return

    def createCameraInfo(self):
        # create a camera model which has 45° openingangle to each image boarder
        camera_info = CameraInfo()
        camera_info.header = Header()
        camera_pixel_width = 100
        camera_pixel_height = 100
        camera_sensor_size_width = 2.0
        camera_sensor_size_height = 2.0
        camera_focal_length = 1.0
        camera_pixel_focal_length_x = (
            camera_focal_length * camera_pixel_width / camera_sensor_size_width
        )
        camera_pixel_focal_length_y = (
            camera_focal_length * camera_pixel_height / camera_sensor_size_height
        )
        camera_info.K = [
            camera_pixel_focal_length_x,
            0.0,
            camera_pixel_width / 2.0,
            0.0,
            camera_pixel_focal_length_y,
            camera_pixel_height / 2.0,
            0.0,
            0.0,
            1.0,
        ]
        camera_info.P = [
            camera_pixel_focal_length_x,
            0.0,
            camera_pixel_width / 2.0,
            0.0,
            0.0,
            camera_pixel_focal_length_y,
            camera_pixel_height / 2.0,
            0.0,
            0.0,
            0.0,
            1.0,
            0.0,
        ]
        camera_info.width = camera_sensor_size_width
        camera_info.height = camera_sensor_size_height
        return camera_info

    """ #broken for yet unknown reasons
    def test_getPixelGPSPosOnWater_noRoll(self):
        from searchwing_common_py.imagePointTransformations import  getPixelGPSPosOnWater

        stamp = rospy.Time.now()

        #Get 3D-Position of drone in local NED coordinates
        drone3dMapPos = PointStamped()
        drone3dMapPos.point.x = 0
        drone3dMapPos.point.y = 0
        drone3dMapPos.point.z = 100
        drone3dMapPos.header.frame_id = "map"
        drone3dMapPos.header.stamp = stamp

        cameraTfName = "cam"
        br = tf.TransformBroadcaster()
        time.sleep(0.5)
        br.sendTransform((0,0,100), \
            tf.transformations.quaternion_from_euler(0,0,0), \
            stamp,"base_link","map")
        time.sleep(0.5)
        br.sendTransform((0,0,0), \
            tf.transformations.quaternion_from_euler(0,np.pi/2.0,0), \
            stamp,"cam_base","base_link")
        time.sleep(0.5)
        br.sendTransform((0,0,0), \
            tf.transformations.quaternion_from_euler(-np.pi/2.0,0,-np.pi/2.0), \
            stamp,cameraTfName,"cam_base")
        time.sleep(1.0)

        self.tf_listener.waitForTransform(
            "map", cameraTfName, stamp, rospy.Duration(3)
        )

        # image center => point to straight to ground
        pt2D = np.array([50,50],int)
        ptGPS, valid = getPixelGPSPosOnWater(pt2D, drone3dMapPos, self.localGpsPos, cameraTfName, self.camModel, stamp)
        self.assertAlmostEqual(ptGPS[0],self.localGpsPos.latitude,places=7)
        self.assertAlmostEqual(ptGPS[1],self.localGpsPos.longitude,places=7)
        self.assertAlmostEqual(ptGPS[2],0.0)
        self.assertTrue(valid)

        # image center top / 45° => 100m to the north
        pt2D = np.array([50,0],int)
        ptGPS, valid = getPixelGPSPosOnWater(pt2D, drone3dMapPos, self.localGpsPos, cameraTfName, self.camModel, stamp)
        self.assertAlmostEquals(ptGPS[0],52.52436219443437,places=5) # 5 digits == ~1.1m  accuarcy
        self.assertAlmostEqual(ptGPS[1],13.420196571909203,places=5)
        self.assertAlmostEqual(ptGPS[2],0.0)
        self.assertTrue(valid)

        # image center right / 45° => 100m to the east
        pt2D = np.array([100,50],int)
        ptGPS, valid = getPixelGPSPosOnWater(pt2D, drone3dMapPos, self.localGpsPos, cameraTfName, self.camModel, stamp)
        self.assertAlmostEquals(ptGPS[0],52.52346882218126,places=5)
        self.assertAlmostEqual(ptGPS[1],13.421674571966859,places=5)
        self.assertAlmostEqual(ptGPS[2],0.0)
        self.assertTrue(valid)

        # image center left / 45° => 100m to the west
        pt2D = np.array([0,50],int)
        ptGPS, valid = getPixelGPSPosOnWater(pt2D, drone3dMapPos, self.localGpsPos, cameraTfName, self.camModel, stamp)
        self.assertAlmostEqual(ptGPS[0],52.52346484,places=5)
        self.assertAlmostEqual(ptGPS[1],13.41871806,places=5)
        self.assertAlmostEqual(ptGPS[2],0.0)
        self.assertTrue(valid)

        # image center bottom / 45° => 100m to the south
        pt2D = np.array([50,100],int)
        ptGPS, valid = getPixelGPSPosOnWater(pt2D, drone3dMapPos, self.localGpsPos, cameraTfName, self.camModel, stamp)
        self.assertAlmostEquals(ptGPS[0],52.522565123424584,places=5)
        self.assertAlmostEqual(ptGPS[1],13.420200640346868,places=5)
        self.assertAlmostEqual(ptGPS[2],0.0)
        self.assertTrue(valid)
    """
    """ #broken for yet unknown reasons
    def test_getPixelGPSPosOnWater_droneRolled(self):
        from searchwing_common_py.imagePointTransformations import  getPixelGPSPosOnWater, getPixel3DPosOnWater

        stamp = rospy.Time.now()

        #Get 3D-Position of drone in local NED coordinates
        drone3dMapPos = PointStamped()
        drone3dMapPos.point.x = 0
        drone3dMapPos.point.y = 0
        drone3dMapPos.point.z = 100
        drone3dMapPos.header.frame_id = "map"
        drone3dMapPos.header.stamp = stamp

        cameraTfName = "cam"
        br = tf.TransformBroadcaster()
        time.sleep(0.5)
        # drone 45° rolled to the right => cam looks to the left
        br.sendTransform((0,0,100),  \
            tf.transformations.quaternion_from_euler(np.pi/4.0,0,0), \
            stamp,"base_link","map")
        time.sleep(0.5)
        br.sendTransform((0,0,0), \
            tf.transformations.quaternion_from_euler(0,np.pi/2.0,0), \
            stamp,"cam_base","base_link")
        time.sleep(0.5)
        br.sendTransform((0,0,0), \
            tf.transformations.quaternion_from_euler(-np.pi/2.0,0,-np.pi/2.0), \
            stamp,cameraTfName,"cam_base" )
        time.sleep(1.0)

        self.tf_listener.waitForTransform(
            "map", cameraTfName, stamp, rospy.Duration(3) # TODO use cam tf from camera info
        )

        # image center right side / 45° => point to straight to ground
        pt2D = np.array([100,50],int)
        ptGPS, valid = getPixelGPSPosOnWater(pt2D, drone3dMapPos, self.localGpsPos, cameraTfName, self.camModel, stamp)
        self.assertAlmostEqual(ptGPS[0],self.localGpsPos.latitude,places=7)
        self.assertAlmostEqual(ptGPS[1],self.localGpsPos.longitude,places=7)
        self.assertAlmostEqual(ptGPS[2],0.0)
        self.assertTrue(valid)

        # image center / 0° => 100m to the west
        pt2D = np.array([50,50],int)
        ptGPS, valid = getPixelGPSPosOnWater(pt2D, drone3dMapPos, self.localGpsPos, cameraTfName, self.camModel, stamp)
        self.assertAlmostEqual(ptGPS[0],52.52346484,places=5)
        self.assertAlmostEqual(ptGPS[1],13.41871806,places=5)
        self.assertAlmostEqual(ptGPS[2],0.0)
        self.assertTrue(valid)

        # image center left side / 45° => looks at Horizon => Error
        pt2D = np.array([0,50],int)
        pt3D, valid = getPixel3DPosOnWater(pt2D, drone3dMapPos, cameraTfName, self.camModel, stamp)
        self.assertFalse(valid)
        ptGPS, valid = getPixelGPSPosOnWater(pt2D, drone3dMapPos, self.localGpsPos, cameraTfName, self.camModel, stamp)
        self.assertFalse(valid)
    """

    def test_getPixelGPSPosOnWater_droneRolledRightAboveHorizon(self):
        from searchwing_common_py.imagePointTransformations import (
            getPixel3DPosOnWater,
            getPixelGPSPosOnWater,
        )

        stamp = rospy.Time.now()

        # Get 3D-Position of drone in local NED coordinates
        drone3dMapPos = PointStamped()
        drone3dMapPos.point.x = 0
        drone3dMapPos.point.y = 0
        drone3dMapPos.point.z = 100
        drone3dMapPos.header.frame_id = "map"
        drone3dMapPos.header.stamp = stamp

        cameraTfName = "cam"
        br = tf.TransformBroadcaster()
        time.sleep(0.5)
        # drone 45° rolled to the right => cam looks to the left
        br.sendTransform(
            (0, 0, 100),
            tf.transformations.quaternion_from_euler(np.pi / 4 + 0.001, 0, 0),
            stamp,
            "base_link",
            "map",
        )
        time.sleep(0.5)
        br.sendTransform(
            (0, 0, 0),
            tf.transformations.quaternion_from_euler(0, np.pi / 2.0, 0),
            stamp,
            "cam_base",
            "base_link",
        )
        time.sleep(0.5)
        br.sendTransform(
            (0, 0, 0),
            tf.transformations.quaternion_from_euler(-np.pi / 2.0, 0, -np.pi / 2.0),
            stamp,
            cameraTfName,
            "cam_base",
        )
        time.sleep(1.0)

        self.tf_listener.waitForTransform(
            "map",
            cameraTfName,
            stamp,
            rospy.Duration(3),  # TODO use cam tf from camera info
        )

        # image center right side / 45° => point to straight to ground
        pt2D = np.array([100, 50], int)
        ptGPS, valid = getPixelGPSPosOnWater(
            pt2D, drone3dMapPos, self.localGpsPos, cameraTfName, self.camModel, stamp
        )
        self.assertTrue(valid)

        # image center / 0° => 100m to the west
        pt2D = np.array([50, 50], int)
        ptGPS, valid = getPixelGPSPosOnWater(
            pt2D, drone3dMapPos, self.localGpsPos, cameraTfName, self.camModel, stamp
        )
        self.assertTrue(valid)

        # image center left side  / >45° => looks slightly above at Horizon => invalid GPS
        pt2D = np.array([0, 50], int)
        pt3D, valid = getPixel3DPosOnWater(
            pt2D, drone3dMapPos, cameraTfName, self.camModel, stamp
        )
        self.assertFalse(valid)
        ptGPS, valid = getPixelGPSPosOnWater(
            pt2D, drone3dMapPos, self.localGpsPos, cameraTfName, self.camModel, stamp
        )
        self.assertFalse(valid)

    def test_getPixelGPSPosOnWater_droneRolledLeftAboveHorizon(self):
        from searchwing_common_py.imagePointTransformations import (
            getPixel3DPosOnWater,
            getPixelGPSPosOnWater,
        )

        stamp = rospy.Time.now()

        # Get 3D-Position of drone in local NED coordinates
        drone3dMapPos = PointStamped()
        drone3dMapPos.point.x = 0
        drone3dMapPos.point.y = 0
        drone3dMapPos.point.z = 100
        drone3dMapPos.header.frame_id = "map"
        drone3dMapPos.header.stamp = stamp

        cameraTfName = "cam"
        br = tf.TransformBroadcaster()
        time.sleep(0.5)
        # drone 45° rolled to the left => cam looks to the right
        br.sendTransform(
            (0, 0, 100),
            tf.transformations.quaternion_from_euler(-np.pi / 4 - 0.001, 0, 0),
            stamp,
            "base_link",
            "map",
        )
        time.sleep(0.5)
        br.sendTransform(
            (0, 0, 0),
            tf.transformations.quaternion_from_euler(0, np.pi / 2.0, 0),
            stamp,
            "cam_base",
            "base_link",
        )
        time.sleep(0.5)
        br.sendTransform(
            (0, 0, 0),
            tf.transformations.quaternion_from_euler(-np.pi / 2.0, 0, -np.pi / 2.0),
            stamp,
            cameraTfName,
            "cam_base",
        )
        time.sleep(1.0)

        self.tf_listener.waitForTransform(
            "map",
            cameraTfName,
            stamp,
            rospy.Duration(3),  # TODO use cam tf from camera info
        )

        # image center left side / 45° => point to straight to ground
        pt2D = np.array([0, 50], int)
        ptGPS, valid = getPixelGPSPosOnWater(
            pt2D, drone3dMapPos, self.localGpsPos, cameraTfName, self.camModel, stamp
        )
        self.assertTrue(valid)

        # image center / 0° => 100m to the east
        pt2D = np.array([50, 50], int)
        ptGPS, valid = getPixelGPSPosOnWater(
            pt2D, drone3dMapPos, self.localGpsPos, cameraTfName, self.camModel, stamp
        )
        self.assertTrue(valid)

        # image center right side  / >45° => looks slightly above at Horizon => invalid GPS
        pt2D = np.array([100, 50], int)
        pt3D, valid = getPixel3DPosOnWater(
            pt2D, drone3dMapPos, cameraTfName, self.camModel, stamp
        )
        self.assertFalse(valid)
        ptGPS, valid = getPixelGPSPosOnWater(
            pt2D, drone3dMapPos, self.localGpsPos, cameraTfName, self.camModel, stamp
        )
        self.assertFalse(valid)


if __name__ == "__main__":
    rostest.rosrun(
        "searchwing_common_py",
        "ImagePointTransformationsTest",
        ImagePointTransformationsTestCase,
    )
