# ros-generic

Implementation of the ROS payload software for the SearchWing drone.

Features:

- Take photos
- Add metadata like GPS positions to photos
- Save photos to disk
- Auto Preflight check
- Save binlogs

## Architecture

Visualisation of the software/hardware architecture: https://miro.com/app/board/uXjVOoT_6rk=/

## Setup

### Install ROS

If you have not yet. See tutorial for your platform on http://wiki.ros.org/ROS/Installation .

- Ubuntu 20.04: Very easy to use using precompiled packages via apt
- Raspberry OS: For install / compile on a RPi checkout 'tools/step_x.sh' scripts

### Create workspace and download private repos

SSH:

```
mkdir -p catkin_ws/src && cd catkin_ws/src
git clone git@gitlab.com:searchwing/development/payloads/ros-generic.git
cd ..
```

HTTPS:

```
mkdir -p catkin_ws/src && cd catkin_ws/src
git clone https://gitlab.com/searchwing/development/payloads/ros-generic.git
cd ..
```

### Download public dependencies

```
cd catkin_ws/src
sudo apt update
sudo rosdep init
rosdep update
rosdep install --from-paths . --ignore-src -y
pip install -r ros-generic/requirements.txt
```

### Compile

#### Pi Platform

```
source /opt/ros/noetic/setup.bash
rm -rf devel/ build/ install/
catkin_make
source devel/setup.bash
```

#### Non-Pi Platform

```
cd catkin_ws
source /opt/ros/noetic/setup.bash
sudo apt-get install python3-catkin-tools -y
catkin config --blacklist searchwing_retinanet
catkin build
source devel/setup.bash
```

- If you are using `zsh` shell, use `source devel/setup.zsh` instead.

- If you are running in a non-NVIDIA-Jetson environment, remove 'searchwing_retinanet' or disable it
  for your catkin build to complete compilation process.

It is recommended to follow the unit testing section to validate that the installation was done
properly.

## Run

### Input

Get all inputs on a raspberry platform according to architecture by running

```
roslaunch searchwing_bringup in.launch
```

### Processing

Start processing of input data

```
roslaunch searchwing_bringup proc.launch
```

### ArduPilot Simulator

If you dont have a FCU on your hand you can also run a simulated ArduPlane on a generic computer.

Then you can connect the ROS environment to it

- Use ArduPilot SITL on a Laptop:
  https://ardupilot.org/dev/docs/sitl-simulator-software-in-the-loop.html#sitl-simulator-software-in-the-loop
- Start SITL and connect via USB-Serial Adapter (/dev/ttyUSB0) to a external Hardware Running the
  ROS Env (e.g. a Pi companion computer):

```
python sim_vehicle.py -w -v ArduPlane  --console --map -A "--uartC=uart:/dev/ttyUSB0:500000"
```

- (Alternative) Connect to SITL on the same machine:

Change the connection in `ros-generic/searchwing_bringup/launch/in_apm.launch` to

```
<arg name="fcu_url" default="udp://127.0.0.1:14551@14555" />
```

- Run mission in SITL

```
MANUAL> wp load Generic_Missions/CMAC-circuit.txt
MANUAL> arm throttle
```

- More details in the [official Tutorial](https://ardupilot.org/dev/docs/ros-sitl.html).

## Test

### Unit testing

All tests can be run with

```
rostest searchwing_common_py testAll.test
```

### Pre-commit hooks for autoformating

Install pre-commit hooks in your environment:

```sh
pre-commit install
```

Optionally test by running the checks on all files:

```sh
pre-commit run --all-files
```

Hint: In case you want to skip the hooks for an individual commit, use the `-n` flag:

```sh
git commit -n
```

## VSCode - Devcontainer

Visual Studio Code uses the files located in the `.devcontainer` directory to build a docker image,
and start a development container.

### Usage

- Replace `HOSTNAME` in `devcontainer/connect_container.sh` with your local hostname.
- Open the `ros-generic` folder as devcontainer using vscode.
- Follow this readme from section `Download public dependencies`.

### Mount external directories

If you want to access extra files from your host machine, you need to add the fllowing lines to the
devcontainer.json:

```
...
"mounts": [
	...
	{
		"source": "path-to-directory-on-host",
		"target": "path-to-directory-in-container",
		"type": "bind"
	}
]
...
```

### Huge memory consumption

If you see huge memory consumption (my roscore needed 8GB RAM) within the container you can fix this
by following [this thread](https://answers.ros.org/question/336963/rosout-high-memory-usage/).

I fixed it by

```
echo 'ulimit -Hn 524288' >> ~/.zshrc
```

### Rootless docker / Manual file permissions

Manual file permission changes must be done when using
[Rootless Docker](https://docs.docker.com/engine/security/rootless/). See
[issue](https://github.com/microsoft/vscode-remote-release/issues/5140#issuecomment-854731774).
